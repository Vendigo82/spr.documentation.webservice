﻿using System;

namespace SPR.Documentation.DataContract
{
    /// <summary>
    /// Contains id
    /// </summary>
    public class IdModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }
    }
}
