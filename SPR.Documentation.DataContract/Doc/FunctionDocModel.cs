﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.DataContract.Doc
{
    public class FunctionDocModel
	{
		/// <summary>
		/// Function's name
		/// </summary>
		public string SystemName { get; set; } = null!;

		/// <summary>
		/// Function's title
		/// </summary>
		public string Title { get; set; } = null!;

		//public IEnumerable<string> Groups { get; set; } = null!;

		public IEnumerable<FunctionImplementationDocModel> Implementations { get; set; } = null!;
    }
}
