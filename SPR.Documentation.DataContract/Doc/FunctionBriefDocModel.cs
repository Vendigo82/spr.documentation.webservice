﻿
namespace SPR.Documentation.DataContract.Doc
{
    public class FunctionBriefDocModel
    {
        /// <summary>
        /// Function's name
        /// </summary>
        public string SystemName { get; set; } = null!;

        /// <summary>
        /// Function's title
        /// </summary>
        public string Title { get; set; } = null!;
    }
}
