﻿
using System.Collections.Generic;

namespace SPR.Documentation.DataContract.Doc
{
    public class FunctionImplementationDocModel
    {
		/// <summary>
		/// Function implementation description
		/// </summary>
		public string Description { get; set; } = null!;

		/// <summary>
		/// Function arguments
		/// </summary>
		public IEnumerable<Editor.ArgumentModel> Args { get; set; } = null!;		
		
		/// <summary>
		/// Function return type
		/// </summary>
		public Editor.ReturnValueModel? Return { get; set; }
	}
}
