﻿
namespace SPR.Documentation.DataContract.Doc
{
    public class GroupDocModel
    {
        public string Group { get; set; } = null!;

        public string Title { get; set; } = null!;
    }
}
