﻿using System.Collections.Generic;

namespace SPR.Documentation.DataContract
{
    /// <summary>
    /// Standard response which contains list of items
    /// </summary>
    /// <typeparam name="T">item type</typeparam>
    public class ItemsContainer<T>
    {
        /// <summary>
        /// List of items
        /// </summary>
        public IEnumerable<T> Items { get; set; } = null!;
    }
}
