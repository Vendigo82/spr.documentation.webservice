﻿
namespace SPR.Documentation.DataContract
{
    /// <summary>
    /// Startdard response for partial list of items
    /// </summary>
    /// <typeparam name="T">Type of item</typeparam>
    public class PartialItemsContainer<T> : ItemsContainer<T>
    {
        /// <summary>
        /// Token for request next data partition
        /// </summary>
        public string? NextToken { get; set; }

        /// <summary>
        /// All data was extracted
        /// </summary>
        public bool Exhausted { get; set; }
    }
}
