﻿using System.Collections.Generic;

namespace SPR.Documentation.DataContract.Editor
{
    public class GroupModel : GroupBriefModel
    {
        /// <summary>
        /// Group titles on different languages
        /// </summary>
        public IEnumerable<TextResource> Titles { get; set; } = null!;
    }
}
