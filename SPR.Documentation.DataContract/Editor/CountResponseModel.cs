﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.DataContract.Editor
{
    /// <summary>
    /// Count response
    /// </summary>
    public class CountResponseModel
    {
        /// <summary>
        /// Count of items
        /// </summary>
        public int Count { get; set; }
    }
}
