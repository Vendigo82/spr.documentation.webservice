﻿using System;

namespace SPR.Documentation.DataContract.Editor
{
    public class GroupBriefModel
    {
        /// <summary>
        /// Group id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Group's short name
        /// </summary>
        public string Alias { get; set; } = null!;
    }
}
