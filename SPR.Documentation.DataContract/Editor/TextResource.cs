﻿
namespace SPR.Documentation.DataContract.Editor
{
    /// <summary>
    /// Text record
    /// </summary>
    public class TextResource
    {
        /// <summary>
        /// Text record's language
        /// </summary>
        public string Lang { get; set; } = null!;

        /// <summary>
        /// Text
        /// </summary>
        public string Text { get; set; } = null!;
    }
}
