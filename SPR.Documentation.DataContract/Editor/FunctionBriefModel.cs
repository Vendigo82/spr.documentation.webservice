﻿using System;

namespace SPR.Documentation.DataContract.Editor
{
    /// <summary>
    /// Function brief info
    /// </summary>
    public class FunctionBriefModel
    {
        /// <summary>
        /// Function's id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Function's system name
        /// </summary>
        public string SystemName { get; set; } = null!;

        /// <summary>
        /// Function's argument count
        /// </summary>
        public short ArgsCount { get; set; }
    }
}
