﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.DataContract.Editor
{
    /// <summary>
    /// Function model
    /// </summary>
    public class FunctionModel : FunctionBriefModel
    {
        /// <summary>
        /// Function titles on different languages
        /// </summary>
        public IEnumerable<TextResource> Titles { get; set; } = null!;

        /// <summary>
        /// Function descriptions on different languages
        /// </summary>
        public IEnumerable<TextResource> Descriptions { get; set; } = null!;

        /// <summary>
        /// List of arguments
        /// </summary>
        public IEnumerable<ArgumentModel> Arguments { get; set; } = null!;

        /// <summary>
        /// Returns value
        /// </summary>
        public ReturnValueModel? Return { get; set; }

        /// <summary>
        /// Library version which function appears
        /// </summary>
        public string? LibraryVersion { get; set; }

        /// <summary>
        /// List of groups which function contains in
        /// If null on update operation, then groups leave unchanged
        /// </summary>
        public IEnumerable<Guid>? Groups { get; set; }
    }
}
