﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.DataContract.Editor
{
    /// <summary>
    /// Function argument
    /// </summary>
    public class ArgumentModel
    {
        /// <summary>
        /// Argument's type
        /// </summary>
        public string Type { get; set; } = null!;

        /// <summary>
        /// Argument's name
        /// </summary>
        public string Name { get; set; } = null!;
    }
}
