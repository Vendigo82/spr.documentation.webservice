﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.DataContract.Editor
{
    /// <summary>
    /// Function return's value model
    /// </summary>
    public class ReturnValueModel
    {
        /// <summary>
        /// Function return's type
        /// </summary>
        public string Type { get; set; } = null!;
    }
}
