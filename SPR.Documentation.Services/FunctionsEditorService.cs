﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using SPR.Documentation.DAL;
using SPR.Documentation.DataContract.Editor;
using SPR.Documentation.Services.Abstractions;
using SPR.Documentation.Services.Exceptions;
using SPR.Documentation.Services.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.Documentation.Services
{
    public class FunctionsEditorService : EditorServiceBase, IFunctionsEditorService
    {
        public FunctionsEditorService(DocContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public async Task<IEnumerable<FunctionBriefModel>> GetFunctionsListAsync(FunctionsRequestOptions options)
        {
            var query = context.Functions.AsNoTracking();
            if (options.StartsFrom != null)
                query = query.Where(f => string.Compare(f.FunctionName.SystemName, options.StartsFrom.SystemName) > 0 
                || (string.Compare(f.FunctionName.SystemName, options.StartsFrom.SystemName) == 0
                    && f.ArgsCount > options.StartsFrom.ArgsCount));

            if (options.Filter != null)
                query = query.Where(i => i.FunctionName.SystemName.Contains(options.Filter));

            query = query
                .OrderBy(i => i.FunctionName.SystemName)
                .ThenBy(i => i.ArgsCount);

            if (options.Offset != null)
                query = query.Skip(options.Offset.Value);

            if (options.MaxCount != null)
                query = query.Take(options.MaxCount.Value);

            return await query.ProjectTo<FunctionBriefModel>(mapper.ConfigurationProvider).ToListAsync();
        }

        public Task<int> GetFunctionsCountAsync(string? filter)
        {
            var query = context.Functions.AsNoTracking();

            if (filter != null)
                query = query.Where(i => i.FunctionName.SystemName.Contains(filter));

            return query.CountAsync();
        }

        public async Task<FunctionModel?> GetFunctionAsync(Guid id)
        {
            var dbitem = await context
                .Functions
                .AsNoTracking()
                .Include(p => p.FunctionName)
                    .ThenInclude(p => p.TitleResource)
                        .ThenInclude(p => p.Rows)
                            .ThenInclude(p => p.Language)
                .Include(p => p.FunctionName)
                    .ThenInclude(p => p.Groups)
                .Include(p => p.DescriptionResource)
                    .ThenInclude(p => p.Rows)
                        .ThenInclude(p => p.Language)
                .Include(p => p.VersionAssignment).ThenInclude(p => p.Version)
                .Where(i => i.Id == id)
                //.ProjectTo<FunctionModel>(_mapper.ConfigurationProvider)
                .SingleOrDefaultAsync();

            return mapper.Map<FunctionModel>(dbitem);
        }

        public async Task<Guid> InsertFunctionAsync(FunctionModel model)
        {
            if (model.Id == Guid.Empty)
                model.Id = Guid.NewGuid();

            var trans = await context.Database.BeginTransactionAsync();

            var langs = await GetLanguagesAsync(model);
            var functionName = await GetOrCreateFunctionNameAsync(langs, model);
            var versionId = await GetLibraryVersionIdAsync(model.LibraryVersion);

            var function = await CreateFunctionAsync();

            // if groups not send (old version) then leave groups untouchable
            if (model.Groups != null)
                await UpdateFunctionNameGroupsAsync(functionName, model.Groups);

            await trans.CommitAsync();

            return function.Id;                              

            async Task<DAL.Model.Function> CreateFunctionAsync()
            {
                var function = mapper.Map<DAL.Model.Function>(model);
                function.FunctionNameId = functionName.Id;
                if (versionId != null)
                    function.VersionAssignment = new DAL.Model.FunctionVersionAssignment { VersionId = versionId.Value };
                function.DescriptionResource = MakeTextResource(langs, model.Descriptions);
                await context.Functions.AddAsync(function);
                await context.SaveChangesAsync();

                return function;
            }
        }

        public async Task UpdateFunctionAsync(FunctionModel model)
        {
            var trans = await context.Database.BeginTransactionAsync();

            var dbitem = await context
                .Functions
                .Include(p => p.DescriptionResource)
                    .ThenInclude(p => p.Rows)
                        .ThenInclude(p => p.Language)
                .Include(p => p.VersionAssignment).ThenInclude(p => p.Version)
                .Where(i => i.Id == model.Id)
                .SingleOrDefaultAsync();
            if (dbitem == null)
                throw new FunctionNotFoundException(model.Id);

            var langs = await GetLanguagesAsync(model);
            var functionName = await GetOrCreateFunctionNameAsync(langs, model);

            mapper.Map(model, dbitem);
            context.Entry(dbitem).Property(p => p.Return).IsModified = true;
            context.Entry(dbitem).Property(p => p.Arguments).IsModified = true;
            dbitem.FunctionNameId = functionName.Id;

            // update version
            var versionId = await GetLibraryVersionIdAsync(model.LibraryVersion);
            UpdateVersion(versionId);

            // update description
            UpdateTextResource(langs, dbitem.DescriptionResource, model.Descriptions);

            // if groups not send (old version) then leave groups untouchable
            if (model.Groups != null)
                await UpdateFunctionNameGroupsAsync(functionName, model.Groups);

            await context.SaveChangesAsync();

            await trans.CommitAsync();

            void UpdateVersion(Guid? versionId)
            {
                if (dbitem.VersionAssignment == null && versionId == null)
                    return;

                if (dbitem.VersionAssignment != null && versionId != null && dbitem.VersionAssignment.VersionId == versionId.Value)
                    return;

                if (versionId == null)
                {
                    if (dbitem.VersionAssignment != null)
                    {
                        context.Entry(dbitem.VersionAssignment).State = EntityState.Deleted;
                        dbitem.VersionAssignment = null;
                    }
                }
                else
                {
                    if (dbitem.VersionAssignment?.VersionId != versionId.Value)
                    {
                        if (dbitem.VersionAssignment != null)
                            context.Entry(dbitem.VersionAssignment).State = EntityState.Deleted;

                        dbitem.VersionAssignment = new DAL.Model.FunctionVersionAssignment { VersionId = versionId.Value };
                    }
                }
            }
        }

        /// <summary>
        /// get list of languages which using in model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private async Task<IEnumerable<DAL.Model.Language>> GetLanguagesAsync(FunctionModel model)
        {
            var codes = model.Titles.Select(i => i.Lang).Concat(model.Descriptions.Select(i => i.Lang)).Distinct().ToArray();
            //var langs = await context.Languages.Where(i => codes.Contains(i.Code)).ToArrayAsync();
            //return langs;
            return await GetLanguagesAsync(codes);
        }

        /// <summary>
        /// get and update or create new function name
        /// </summary>
        /// <param name="langs"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        private async Task<DAL.Model.FunctionName> GetOrCreateFunctionNameAsync(IEnumerable<DAL.Model.Language> langs, FunctionModel model)
        {
            var functionName = await context.FunctionNames
                .Include(i => i.TitleResource)
                    .ThenInclude(i => i.Rows)
                        .ThenInclude(i => i.Language)
                .Include(i => i.Groups)
                .FirstOrDefaultAsync(i => i.SystemName == model.SystemName);

            if (functionName == null)
            {
                functionName = new DAL.Model.FunctionName {
                    SystemName = model.SystemName.Trim(),
                    TitleResource = MakeTextResource(langs, model.Titles),
                    Groups = new List<DAL.Model.FunctionGroup>()
                };
                await context.FunctionNames.AddAsync(functionName);
            }
            else
            {
                UpdateTextResource(langs, functionName.TitleResource, model.Titles);
            }
            await context.SaveChangesAsync();

            return functionName;
        }

        /// <summary>
        /// Returns library version's id or null, if <paramref name="libraryVersion"/> is null 
        /// </summary>
        /// <param name="libraryVersion"></param>
        /// <returns></returns>
        private async Task<Guid?> GetLibraryVersionIdAsync(string? libraryVersion)
        {
            if (libraryVersion == null)
                return null;

            var version = libraryVersion.Split('.').Select(i => int.Parse(i)).ToArray();

            var versionId = await context
                .Versions
                .Where(i => i.VersionValue.SequenceEqual(version))
                .Select(i => i.Id)
                .FirstOrDefaultAsync();

            if (versionId == Guid.Empty)
                throw new LibraryVersionNotFoundException(libraryVersion);

            return versionId;
        }

        private async Task UpdateFunctionNameGroupsAsync(DAL.Model.FunctionName functionName, IEnumerable<Guid> groups)
        {
            var addGroupsIds = groups.Except(functionName.Groups.Select(i => i.Id)).ToArray();
            var delGroups = functionName.Groups.Where(i => !groups.Contains(i.Id)).ToArray();

            foreach (var del in delGroups)
                functionName.Groups.Remove(del);

            var addGroups = await context.FunctionGroups.Where(i => addGroupsIds.Contains(i.Id)).ToArrayAsync();
            var notFoundGroups = addGroupsIds.Except(addGroups.Select(i => i.Id)).ToArray();
            if (notFoundGroups.Any())
                throw new GroupNotFoundException(notFoundGroups);

            foreach (var add in addGroups)
                functionName.Groups.Add(add);

            await context.SaveChangesAsync();
        }

        public Task<IEnumerable<string>> GetDataTypesAsync()
        {
            var list = Enum.GetValues<DAL.Model.DataTypeEnum>().AsEnumerable().Select(i => i.ToString());
            return Task.FromResult(list);
        }
    }
}
