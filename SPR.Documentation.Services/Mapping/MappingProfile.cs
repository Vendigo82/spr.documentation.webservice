﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.Services.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<DAL.Model.Function, DataContract.Editor.FunctionBriefModel>()
                .ForMember(d => d.SystemName, m => m.MapFrom(s => s.FunctionName.SystemName))
                .ForMember(d => d.ArgsCount, m => m.MapFrom(s => s.ArgsCount));

            CreateMap<DAL.Model.TextResourceRow, DataContract.Editor.TextResource>()
                .ForMember(d => d.Lang, m => m.MapFrom(s => s.Language.Code))
                .ForMember(d => d.Text, m => m.MapFrom(s => s.Text));

            CreateMap<DAL.Model.DataTypeEnum, string>().ConvertUsing(t => t.ToString());
            CreateMap<string, DAL.Model.DataTypeEnum>().ConvertUsing(t => Enum.Parse<DAL.Model.DataTypeEnum>(t));

           // CreateMap<int[]?, string>().ConvertUsing(t => t != null ? string.Join('.', t) : null);

            CreateMap<DAL.Model.FunctionArgument, DataContract.Editor.ArgumentModel>()
                .ReverseMap();
            CreateMap<DAL.Model.FunctionReturnValue, DataContract.Editor.ReturnValueModel>()
                .ReverseMap();

            CreateMap<DAL.Model.Function, DataContract.Editor.FunctionModel>()
                .IncludeBase<DAL.Model.Function, DataContract.Editor.FunctionBriefModel>()
                .ForMember(d => d.Titles, m => m.MapFrom(s => s.FunctionName.TitleResource.Rows))
                .ForMember(d => d.Descriptions, m => m.MapFrom(s => s.DescriptionResource.Rows))
                .ForMember(d => d.Arguments, m => m.MapFrom(s => s.Arguments))
                .ForMember(d => d.LibraryVersion, m => m.MapFrom(s => s.VersionAssignment != null ? string.Join('.', s.VersionAssignment.Version.VersionValue) : null))
                .ForMember(d => d.Groups, m => m.MapFrom(s => s.FunctionName.Groups.Select(i => i.Id)))
                ;

            CreateMap<DataContract.Editor.FunctionModel, DAL.Model.Function>()
                .ForMember(d => d.FunctionNameId, m => m.Ignore())
                .ForMember(d => d.FunctionName, m => m.Ignore())
                .ForMember(d => d.DescriptionResourceId, m => m.Ignore())
                .ForMember(d => d.DescriptionResource, m => m.Ignore())
                .ForMember(d => d.Examples, m => m.Ignore())
                .ForMember(d => d.VersionAssignment, m => m.Ignore())
                ;

            CreateMap<DAL.Model.FunctionGroup, DataContract.Editor.GroupBriefModel>();
            CreateMap<DAL.Model.FunctionGroup, DataContract.Editor.GroupModel>()
                .IncludeBase<DAL.Model.FunctionGroup, DataContract.Editor.GroupBriefModel>()
                .ForMember(d => d.Titles, m => m.MapFrom(s => s.TitleResource.Rows));

            CreateMap<DataContract.Editor.GroupModel, DAL.Model.FunctionGroup>()
                .ForMember(d => d.Functions, m => m.Ignore())
                .ForMember(d => d.TitleResource, m => m.Ignore())
                .ForMember(d => d.TitleResourceId, m => m.Ignore());

            CreateMap<Model.QueryModels.FunctionImplementationQueryModel, DataContract.Doc.FunctionImplementationDocModel>();
            CreateMap<Model.QueryModels.FunctionQueryModel, DataContract.Doc.FunctionDocModel>()
                .ForMember(d => d.Implementations, m => m.Ignore());
            CreateMap<(Model.QueryModels.FunctionQueryModel, IEnumerable<Model.QueryModels.FunctionImplementationQueryModel>), DataContract.Doc.FunctionDocModel>()
                .IncludeMembers(p => p.Item1)
                .ForMember(d => d.Implementations, m => m.MapFrom(s => s.Item2));
        }
    }
}
