﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SPR.Documentation.DAL;
using SPR.Documentation.DataContract.Editor;
using SPR.Documentation.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.Services
{
    public class EditorServiceBase
    {
        protected readonly DocContext context;
        protected readonly IMapper mapper;

        public EditorServiceBase(DocContext context, IMapper mapper)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Get list of languages which using in model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        protected async Task<IEnumerable<DAL.Model.Language>> GetLanguagesAsync(IEnumerable<string> codes)
        {
            var langs = await context.Languages.AsNoTracking().Where(i => codes.Contains(i.Code)).ToArrayAsync();
            return langs;
        }


        /// <summary>
        /// Get language id
        /// </summary>
        /// <param name="langs"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        /// <exception cref="LanguageNotFoundException">language with given code does not found</exception>
        protected static Guid GetLanguageId(IEnumerable<DAL.Model.Language> langs, string code)
                => langs.FirstOrDefault(lang => lang.Code == code)?.Id ?? throw new LanguageNotFoundException(code);

        /// <summary>
        /// Make text resouce
        /// </summary>
        /// <param name="langs"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        /// <exception cref="LanguageNotFoundException">language with given code does not found</exception>
        protected static DAL.Model.TextResource MakeTextResource(IEnumerable<DAL.Model.Language> langs, IEnumerable<TextResource> text)
        {
            return new DAL.Model.TextResource {
                Rows = text.Select(i => new DAL.Model.TextResourceRow {
                    LanguageId = GetLanguageId(langs, i.Lang),
                    Text = i.Text
                }).ToList()
            };
        }

        protected void UpdateTextResource(IEnumerable<DAL.Model.Language> langs, DAL.Model.TextResource dbresource, IEnumerable<TextResource> texts)
        {
            var newLangs = texts.Select(i => i.Lang).ToArray();

            var deleting = dbresource.Rows.Where(i => !newLangs.Contains(i.Language.Code)).ToArray();
            foreach (var item in deleting)
                context.Entry(item).State = EntityState.Deleted;

            var existedDbRows = dbresource.Rows.ToArray();
            foreach (var item in texts)
            {
                var existedDbItem = existedDbRows.FirstOrDefault(i => i.Language.Code == item.Lang);
                if (existedDbItem != null)
                {
                    if (existedDbItem.Text != item.Text)
                        existedDbItem.Text = item.Text;
                }
                else
                    dbresource.Rows.Add(new DAL.Model.TextResourceRow { LanguageId = GetLanguageId(langs, item.Lang), Text = item.Text });
            }
        }
    }
}
