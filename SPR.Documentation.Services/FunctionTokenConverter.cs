﻿using SPR.Documentation.DataContract.Editor;
using SPR.Documentation.Services.Abstractions;
using SPR.Documentation.Services.Exceptions;
using System;
using System.Text;

namespace SPR.Documentation.Services
{
    public class FunctionTokenConverter : ITokenConvertor<FunctionBriefModel>
    {
        private const string TokenVersion = "ftoken1";

        public FunctionBriefModel Deserialize(string token)
        {
            try
            {
                var bytes = Convert.FromBase64String(token);
                var plain = Encoding.UTF8.GetString(bytes);
                var components = plain.Split(':');

                if (components.Length != 3)
                    throw new InvalidTokenException("Invalid token");

                if (components[0] != TokenVersion)
                    throw new InvalidTokenException("Invalid token version");

                if (!short.TryParse(components[2], out var cnt))
                    throw new InvalidTokenException("Invalid token values");

                return new FunctionBriefModel { SystemName = components[1], ArgsCount = cnt };
            } 
            catch (FormatException e)
            {
                throw new InvalidTokenException("Invalid token", e);
            }
        }

        public string Serialize(FunctionBriefModel token)
        {
            var plain = $"{TokenVersion}:{token.SystemName}:{token.ArgsCount}";
            var bytes = Encoding.UTF8.GetBytes(plain);
            return Convert.ToBase64String(bytes);
        }
    }
}
