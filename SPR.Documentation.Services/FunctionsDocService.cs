﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SPR.Documentation.DAL;
using SPR.Documentation.DataContract.Doc;
using SPR.Documentation.Services.Abstractions;
using SPR.Documentation.Services.Model.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.Services
{
    public class FunctionsDocService : IFunctionsDocService
    {
        private readonly DocContext context;
        private readonly IMapper mapper;
        private readonly IOptions<Settings> options;

        public FunctionsDocService(DocContext context, IMapper mapper, IOptions<Settings> options)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.options = options ?? throw new ArgumentNullException(nameof(options));
        }

        public async Task<FunctionDocModel?> GetFunctionAsync(PresentationRequest request, string systemName)
        {
            var prjVer = await GetProjectVersionRequiredAsync(request);

            var func = await context.FunctionNames.AsNoTracking()
                .Where(i => i.SystemName == systemName)
                .Select(i => new FunctionQueryModel
                {
                    Id = i.Id,
                    SystemName = i.SystemName,
                    Title = i.TitleResource.Rows.Where(i => i.Language.Code == request.Language).Select(i => i.Text).FirstOrDefault()
                        ?? i.TitleResource.Rows.Where(i => i.Language.Code == options.Value.DefaultLanguage).Select(i => i.Text).FirstOrDefault()
                        ?? i.TitleResource.Rows.Select(i => i.Text).FirstOrDefault()
                        ?? string.Empty
                }).FirstOrDefaultAsync();

            if (func == null)
                return null;

            var implementations = await context.Functions
                .AsNoTracking()
                .Where(p => p.FunctionNameId == func.Id && p.VersionAssignment != null
                    && (p.VersionAssignment.Version.VersionValue[0] < prjVer[0] || (p.VersionAssignment.Version.VersionValue[0] == prjVer[0] && p.VersionAssignment.Version.VersionValue[1] < prjVer[1])
                        || (p.VersionAssignment.Version.VersionValue[0] == prjVer[0] && p.VersionAssignment.Version.VersionValue[1] == prjVer[1] && p.VersionAssignment.Version.VersionValue[2] <= prjVer[2])))
                .OrderBy(i => i.ArgsCount)
                .Select(i => new FunctionImplementationQueryModel
                {
                    Description = i.DescriptionResource.Rows.Where(i => i.Language.Code == request.Language).Select(i => i.Text).FirstOrDefault()
                        ?? i.DescriptionResource.Rows.Where(i => i.Language.Code == options.Value.DefaultLanguage).Select(i => i.Text).FirstOrDefault()
                        ?? i.DescriptionResource.Rows.Select(i => i.Text).FirstOrDefault()
                        ?? string.Empty,
                    Return = i.Return,
                    Args = i.Arguments
                })
                .ToListAsync();

            if (!implementations.Any())
                return null;

            var result = mapper.Map<FunctionDocModel>((func, implementations.AsEnumerable()));
            return result;
        }

        public async Task<IEnumerable<FunctionBriefDocModel>> GetFunctionsListAsync(PresentationRequest request, string? groupFilter, string? nameFilter)
        {
            var prjVer = await GetProjectVersionRequiredAsync(request);

            // maybe is not most optimized query, but working
            var query = context.FunctionNames.AsNoTracking()
                .Where(i => i.Functions.Any(p => p.VersionAssignment != null
                    && (p.VersionAssignment.Version.VersionValue[0] < prjVer[0] || (p.VersionAssignment.Version.VersionValue[0] == prjVer[0] && p.VersionAssignment.Version.VersionValue[1] < prjVer[1])
                        || (p.VersionAssignment.Version.VersionValue[0] == prjVer[0] && p.VersionAssignment.Version.VersionValue[1] == prjVer[1] && p.VersionAssignment.Version.VersionValue[2] <= prjVer[2]))));

            if (groupFilter != null)
                query = query.Where(i => i.Groups.Any(i => i.Alias == groupFilter));

            if (nameFilter != null)
                query = query.Where(i => i.SystemName.Contains(nameFilter));

            var list = await query
                .OrderBy(i => i.SystemName)
                .Select(i => new FunctionBriefDocModel
                {
                    SystemName = i.SystemName,
                    Title = i.TitleResource.Rows.Where(i => i.Language.Code == request.Language).Select(i => i.Text).FirstOrDefault() 
                        ?? i.TitleResource.Rows.Where(i => i.Language.Code == options.Value.DefaultLanguage).Select(i => i.Text).FirstOrDefault()
                        ?? i.TitleResource.Rows.Select(i => i.Text).FirstOrDefault()
                        ?? string.Empty
                }).ToArrayAsync();

            return list;
        }

        public async Task<IEnumerable<GroupDocModel>> GetGroupsListAsync(PresentationRequest request)
        {
            var list = await context.FunctionGroups.AsNoTracking()
                .OrderBy(i => i.Alias)
                .Select(i => new GroupDocModel
                {
                    Group = i.Alias,
                    Title = i.TitleResource.Rows.Where(i => i.Language.Code == request.Language).Select(i => i.Text).FirstOrDefault()
                        ?? i.TitleResource.Rows.Where(i => i.Language.Code == options.Value.DefaultLanguage).Select(i => i.Text).FirstOrDefault()
                        ?? i.TitleResource.Rows.Select(i => i.Text).FirstOrDefault()
                        ?? string.Empty
                })
                .ToArrayAsync();

            return list;
        }

        public class Settings
        {
            public string DefaultLanguage { get; set; } = "en";
        }

        private async Task<int[]> GetProjectVersionRequiredAsync(PresentationRequest request) 
        {
            var version = request.Version;
            var projectVersion = await context.ProjectVersions.AsNoTracking()
                .Where(i => i.Project.SystemName == request.Project
                    && (i.Version[0] < version[0] || (i.Version[0] == version[0] && i.Version[1] < version[1]) || (i.Version[0] == version[0] && i.Version[1] == version[1] && i.Version[2] <= version[2])))
                .OrderByDescending(i => i.Version)
                .FirstOrDefaultAsync();

            if (projectVersion == null)
                throw new Exceptions.ProjectNotFoundException(request.Project);

            return projectVersion.LibraryVersion;
        }
    }
}
