﻿using System;

namespace SPR.Documentation.Services.Exceptions
{
    public class LibraryVersionNotFoundException : Exception
    {
        public LibraryVersionNotFoundException(string version) : base($"Library version '{version}' does not found")
        {
            Version = version;
        }

        public string Version { get; }
    }
}
