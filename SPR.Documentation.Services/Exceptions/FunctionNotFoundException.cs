﻿using System;

namespace SPR.Documentation.Services.Exceptions
{
    public class FunctionNotFoundException : Exception
    {
        public FunctionNotFoundException(Guid functionId) : base($"Function with id = '{functionId}' does not found")
        {
        }
    }
}
