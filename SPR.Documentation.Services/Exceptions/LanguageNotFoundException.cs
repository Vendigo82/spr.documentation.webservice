﻿using System;

namespace SPR.Documentation.Services.Exceptions
{
    public class LanguageNotFoundException : Exception
    {
        public LanguageNotFoundException(string languageCode) : base($"Language with code '{languageCode}' does not found")
        {
            LanguageCode = languageCode;
        }

        public string LanguageCode { get; }


    }
}
