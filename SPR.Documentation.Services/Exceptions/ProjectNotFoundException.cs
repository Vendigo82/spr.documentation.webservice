﻿using System;

namespace SPR.Documentation.Services.Exceptions
{
    public class ProjectNotFoundException : Exception
    {
        public ProjectNotFoundException(string projectName) : base($"Project {projectName} does not fund")
        {
            ProjectName = projectName;
        }

        public string ProjectName { get; }
    }
}
