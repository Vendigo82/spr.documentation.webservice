﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SPR.Documentation.Services.Exceptions
{
    public class GroupNotFoundException : Exception
    {
        public IEnumerable<Guid> GroupsIds { get; }

        public GroupNotFoundException(Guid groupId) : base($"Group with id = '{groupId}' does not found")
        {
            GroupsIds = Enumerable.Repeat(groupId, 1);
        }

        public GroupNotFoundException(IEnumerable<Guid> groupsIds) : base($"Groups with ids '{string.Join(", ", groupsIds)}' does not found")
        {
            GroupsIds = groupsIds;
        }
    }
}
