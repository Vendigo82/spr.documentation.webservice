﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using SPR.Documentation.DAL;
using SPR.Documentation.DataContract.Editor;
using SPR.Documentation.Services.Abstractions;
using SPR.Documentation.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.Documentation.Services
{
    public class GroupsEditorService : EditorServiceBase, IGroupsEditorService
    {
        public GroupsEditorService(DocContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public async Task<IEnumerable<GroupBriefModel>> GetListAsync()
        {
            return await context
                .FunctionGroups
                .OrderBy(i => i.Alias)
                .ProjectTo<GroupBriefModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public Task<GroupModel?> GetItemAsync(Guid id)
        {
            return context
                .FunctionGroups
                .Where(i => i.Id == id)
                .ProjectTo<GroupModel?>(mapper.ConfigurationProvider)
                .SingleOrDefaultAsync();
        }

        public async Task<Guid> InsertAsync(GroupModel model)
        {
            if (model.Id == Guid.Empty)
                model.Id = Guid.NewGuid();

            var langs = await GetLanguagesAsync(model.Titles.Select(i => i.Lang));
            var dbitem = mapper.Map<DAL.Model.FunctionGroup>(model);
            dbitem.TitleResource = MakeTextResource(langs, model.Titles);
            dbitem.Alias = dbitem.Alias.Trim();

            await context.FunctionGroups.AddAsync(dbitem);
            await context.SaveChangesAsync();

            return dbitem.Id;
        }

        public async Task UpdateAsync(GroupModel model)
        {
            var dbitem = await context
                .FunctionGroups
                .Include(i => i.TitleResource)
                    .ThenInclude(i => i.Rows)
                        .ThenInclude(i => i.Language)
                .FirstOrDefaultAsync(i => i.Id == model.Id);
            if (dbitem == null)
                throw new GroupNotFoundException(model.Id);

            var langs = await GetLanguagesAsync(model.Titles.Select(i => i.Lang));
            mapper.Map(model, dbitem);
            dbitem.Alias = dbitem.Alias.Trim();
            UpdateTextResource(langs, dbitem.TitleResource, model.Titles);

            await context.SaveChangesAsync();
        }
    }
}
