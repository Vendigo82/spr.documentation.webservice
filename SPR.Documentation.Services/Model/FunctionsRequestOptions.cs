﻿using SPR.Documentation.DataContract.Editor;

namespace SPR.Documentation.Services.Model
{
    public class FunctionsRequestOptions
    {
        public FunctionBriefModel? StartsFrom { get; set; }

        public string? Filter { get; set; }

        public int? MaxCount { get; set; }

        /// <summary>
        /// Database offset
        /// </summary>
        public int? Offset { get; set; }
    }
}
