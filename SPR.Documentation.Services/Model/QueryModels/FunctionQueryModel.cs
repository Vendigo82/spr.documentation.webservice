﻿using System;

namespace SPR.Documentation.Services.Model.QueryModels
{
    public class FunctionQueryModel
    {
		public Guid Id { get; set; }

		/// <summary>
		/// Function's name
		/// </summary>
		public string SystemName { get; set; } = null!;

		/// <summary>
		/// Function's title
		/// </summary>
		public string Title { get; set; } = null!;
	}
}
