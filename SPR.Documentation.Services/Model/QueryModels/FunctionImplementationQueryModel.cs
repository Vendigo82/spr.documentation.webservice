﻿using System.Collections.Generic;

namespace SPR.Documentation.Services.Model.QueryModels
{
    public class FunctionImplementationQueryModel
    {
        public string? Description { get; set; }

        public DAL.Model.FunctionReturnValue? Return { get; set; }

        public IEnumerable<DAL.Model.FunctionArgument> Args { get; set; } = null!;
    }
}
