﻿using SPR.Documentation.DataContract.Doc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SPR.Documentation.Services.Abstractions
{
    public record PresentationRequest(string Language, string Project, int[] Version) { }

    public interface IFunctionsDocService
    {
        public Task<IEnumerable<FunctionBriefDocModel>> GetFunctionsListAsync(PresentationRequest request, string? groupFilter, string? nameFilter);

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="systemName"></param>
        /// <returns></returns>
        public Task<FunctionDocModel?> GetFunctionAsync(PresentationRequest request, string systemName);

        public Task<IEnumerable<GroupDocModel>> GetGroupsListAsync(PresentationRequest request);
    }
}
