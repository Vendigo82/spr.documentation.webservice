﻿using SPR.Documentation.DataContract.Editor;
using SPR.Documentation.Services.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.Services.Abstractions
{
    public interface IFunctionsEditorService
    {
        /// <summary>
        /// Return list od available data types
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<string>> GetDataTypesAsync();

        /// <summary>
        /// Load list of functions
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<IEnumerable<FunctionBriefModel>> GetFunctionsListAsync(FunctionsRequestOptions request);

        /// <summary>
        /// Returns count of functions with filter
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public Task<int> GetFunctionsCountAsync(string? filter);

        /// <summary>
        /// Load single function by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Function model or null if not found</returns>
        public Task<FunctionModel?> GetFunctionAsync(Guid id);

        /// <summary>
        /// Insert new function
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Task<Guid> InsertFunctionAsync(FunctionModel model);

        /// <summary>
        /// Update function
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Task UpdateFunctionAsync(FunctionModel model);
    }
}
