﻿
namespace SPR.Documentation.Services.Abstractions
{
    /// <summary>
    /// Provide token serialize and deserialize methods
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ITokenConvertor<T>
    {
        /// <summary>
        /// Deserialize token
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        /// <exception cref="Exceptions.InvalidTokenException"></exception>
        public T Deserialize(string token);

        /// <summary>
        /// Serialize token
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public string Serialize(T token);
    }
}
