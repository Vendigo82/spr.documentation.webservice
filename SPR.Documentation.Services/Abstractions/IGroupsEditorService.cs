﻿using SPR.Documentation.DataContract.Editor;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SPR.Documentation.Services.Abstractions
{
    public interface IGroupsEditorService
    {
        public Task<IEnumerable<GroupBriefModel>> GetListAsync();

        public Task<Guid> InsertAsync(GroupModel model);

        public Task UpdateAsync(GroupModel model);

        public Task<GroupModel?> GetItemAsync(Guid id);
    }
}
