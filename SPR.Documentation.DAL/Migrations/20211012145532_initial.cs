﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using SPR.Documentation.DAL.Model;

namespace SPR.Documentation.DAL.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "doc");

            migrationBuilder.EnsureSchema(
                name: "text");

            migrationBuilder.EnsureSchema(
                name: "project");

            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:PostgresExtension:pgcrypto", ",,");

            migrationBuilder.CreateTable(
                name: "language",
                schema: "text",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "gen_random_uuid()"),
                    code = table.Column<string>(type: "text", nullable: false),
                    title = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_language", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "text_resource",
                schema: "text",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "gen_random_uuid()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_text_resource", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "version",
                schema: "doc",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "gen_random_uuid()"),
                    version_value = table.Column<int[]>(type: "integer[]", nullable: false),
                    created_at = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false, defaultValueSql: "timezone('UTC'::text, now())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_version", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "function_group",
                schema: "doc",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    title_resource_id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_function_group", x => x.id);
                    table.ForeignKey(
                        name: "fk_function_group_text_resource_title_resource_id",
                        column: x => x.title_resource_id,
                        principalSchema: "text",
                        principalTable: "text_resource",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "function_name",
                schema: "doc",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "gen_random_uuid()"),
                    system_name = table.Column<string>(type: "text", nullable: false),
                    title_resource_id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_function_name", x => x.id);
                    table.ForeignKey(
                        name: "fk_function_name_text_resource_title_resource_id",
                        column: x => x.title_resource_id,
                        principalSchema: "text",
                        principalTable: "text_resource",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "project",
                schema: "project",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "gen_random_uuid()"),
                    system_name = table.Column<string>(type: "text", nullable: false),
                    title_resource_id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_project", x => x.id);
                    table.ForeignKey(
                        name: "fk_project_text_resource_title_resource_id",
                        column: x => x.title_resource_id,
                        principalSchema: "text",
                        principalTable: "text_resource",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "text_resource_row",
                schema: "text",
                columns: table => new
                {
                    text_resource_id = table.Column<Guid>(type: "uuid", nullable: false),
                    language_id = table.Column<Guid>(type: "uuid", nullable: false),
                    text = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_text_resource_row", x => new { x.language_id, x.text_resource_id });
                    table.ForeignKey(
                        name: "fk_text_resource_row_language_language_id",
                        column: x => x.language_id,
                        principalSchema: "text",
                        principalTable: "language",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_text_resource_row_text_resource_text_resource_id",
                        column: x => x.text_resource_id,
                        principalSchema: "text",
                        principalTable: "text_resource",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "function",
                schema: "doc",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "gen_random_uuid()"),
                    function_name_id = table.Column<Guid>(type: "uuid", nullable: false),
                    args_count = table.Column<short>(type: "smallint", nullable: false),
                    arguments = table.Column<FunctionArgument[]>(type: "jsonb", nullable: false),
                    @return = table.Column<FunctionReturnValue>(name: "return", type: "jsonb", nullable: true),
                    description_resource_id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_function", x => x.id);
                    table.ForeignKey(
                        name: "fk_function_function_name_function_name_id",
                        column: x => x.function_name_id,
                        principalSchema: "doc",
                        principalTable: "function_name",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_function_text_resource_description_resource_id",
                        column: x => x.description_resource_id,
                        principalSchema: "text",
                        principalTable: "text_resource",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "function_group_function_name",
                schema: "doc",
                columns: table => new
                {
                    functions_id = table.Column<Guid>(type: "uuid", nullable: false),
                    groups_id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_function_group_function_name", x => new { x.functions_id, x.groups_id });
                    table.ForeignKey(
                        name: "fk_function_group_function_name_function_groups_groups_id",
                        column: x => x.groups_id,
                        principalSchema: "doc",
                        principalTable: "function_group",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_function_group_function_name_function_names_functions_id",
                        column: x => x.functions_id,
                        principalSchema: "doc",
                        principalTable: "function_name",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "project_version",
                schema: "project",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "gen_random_uuid()"),
                    project_id = table.Column<Guid>(type: "uuid", nullable: false),
                    version = table.Column<int[]>(type: "integer[]", nullable: false),
                    library_version = table.Column<int[]>(type: "integer[]", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_project_version", x => x.id);
                    table.ForeignKey(
                        name: "fk_project_version_project_project_id",
                        column: x => x.project_id,
                        principalSchema: "project",
                        principalTable: "project",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "example",
                schema: "doc",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "gen_random_uuid()"),
                    function_id = table.Column<Guid>(type: "uuid", nullable: false),
                    example = table.Column<string>(type: "text", nullable: false),
                    order = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_example", x => x.id);
                    table.ForeignKey(
                        name: "fk_example_function_function_id",
                        column: x => x.function_id,
                        principalSchema: "doc",
                        principalTable: "function",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "function_version_assignment",
                schema: "doc",
                columns: table => new
                {
                    function_id = table.Column<Guid>(type: "uuid", nullable: false),
                    version_id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_function_version_assignment", x => new { x.function_id, x.version_id });
                    table.ForeignKey(
                        name: "fk_function_version_assignment_functions_function_id",
                        column: x => x.function_id,
                        principalSchema: "doc",
                        principalTable: "function",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_function_version_assignment_versions_version_id",
                        column: x => x.version_id,
                        principalSchema: "doc",
                        principalTable: "version",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_example_function_id",
                schema: "doc",
                table: "example",
                column: "function_id");

            migrationBuilder.CreateIndex(
                name: "ix_function_description_resource_id",
                schema: "doc",
                table: "function",
                column: "description_resource_id");

            migrationBuilder.CreateIndex(
                name: "ix_function_function_name_id",
                schema: "doc",
                table: "function",
                column: "function_name_id");

            migrationBuilder.CreateIndex(
                name: "unq_function_args_count_function_name_id",
                schema: "doc",
                table: "function",
                columns: new[] { "args_count", "function_name_id" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_function_group_title_resource_id",
                schema: "doc",
                table: "function_group",
                column: "title_resource_id");

            migrationBuilder.CreateIndex(
                name: "ix_function_group_function_name_groups_id",
                schema: "doc",
                table: "function_group_function_name",
                column: "groups_id");

            migrationBuilder.CreateIndex(
                name: "ix_function_name_system_name",
                schema: "doc",
                table: "function_name",
                column: "system_name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_function_name_title_resource_id",
                schema: "doc",
                table: "function_name",
                column: "title_resource_id");

            migrationBuilder.CreateIndex(
                name: "ix_function_version_assignment_function_id",
                schema: "doc",
                table: "function_version_assignment",
                column: "function_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_function_version_assignment_version_id",
                schema: "doc",
                table: "function_version_assignment",
                column: "version_id");

            migrationBuilder.CreateIndex(
                name: "ix_language_code",
                schema: "text",
                table: "language",
                column: "code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_project_system_name",
                schema: "project",
                table: "project",
                column: "system_name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_project_title_resource_id",
                schema: "project",
                table: "project",
                column: "title_resource_id");

            migrationBuilder.CreateIndex(
                name: "ix_project_version_project_id",
                schema: "project",
                table: "project_version",
                column: "project_id");

            migrationBuilder.CreateIndex(
                name: "ix_project_version_version",
                schema: "project",
                table: "project_version",
                column: "version",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_text_resource_row_text_resource_id",
                schema: "text",
                table: "text_resource_row",
                column: "text_resource_id");

            migrationBuilder.CreateIndex(
                name: "ix_version_version_value",
                schema: "doc",
                table: "version",
                column: "version_value",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "example",
                schema: "doc");

            migrationBuilder.DropTable(
                name: "function_group_function_name",
                schema: "doc");

            migrationBuilder.DropTable(
                name: "function_version_assignment",
                schema: "doc");

            migrationBuilder.DropTable(
                name: "project_version",
                schema: "project");

            migrationBuilder.DropTable(
                name: "text_resource_row",
                schema: "text");

            migrationBuilder.DropTable(
                name: "function_group",
                schema: "doc");

            migrationBuilder.DropTable(
                name: "function",
                schema: "doc");

            migrationBuilder.DropTable(
                name: "version",
                schema: "doc");

            migrationBuilder.DropTable(
                name: "project",
                schema: "project");

            migrationBuilder.DropTable(
                name: "language",
                schema: "text");

            migrationBuilder.DropTable(
                name: "function_name",
                schema: "doc");

            migrationBuilder.DropTable(
                name: "text_resource",
                schema: "text");
        }
    }
}
