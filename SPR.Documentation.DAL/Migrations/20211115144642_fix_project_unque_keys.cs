﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SPR.Documentation.DAL.Migrations
{
    public partial class fix_project_unque_keys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "ix_project_version_version",
                schema: "project",
                table: "project_version");

            migrationBuilder.RenameIndex(
                name: "ix_project_system_name",
                schema: "project",
                table: "project",
                newName: "unq_project_system_name");

            migrationBuilder.CreateIndex(
                name: "unq_project_version_project_id_version",
                schema: "project",
                table: "project_version",
                columns: new[] { "version", "project_id" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "unq_project_version_project_id_version",
                schema: "project",
                table: "project_version");

            migrationBuilder.RenameIndex(
                name: "unq_project_system_name",
                schema: "project",
                table: "project",
                newName: "ix_project_system_name");

            migrationBuilder.CreateIndex(
                name: "ix_project_version_version",
                schema: "project",
                table: "project_version",
                column: "version",
                unique: true);
        }
    }
}
