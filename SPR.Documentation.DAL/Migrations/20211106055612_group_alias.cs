﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SPR.Documentation.DAL.Migrations
{
    public partial class group_alias : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "alias",
                schema: "doc",
                table: "function_group",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "unq_function_group_alias",
                schema: "doc",
                table: "function_group",
                column: "alias",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "unq_function_group_alias",
                schema: "doc",
                table: "function_group");

            migrationBuilder.DropColumn(
                name: "alias",
                schema: "doc",
                table: "function_group");
        }
    }
}
