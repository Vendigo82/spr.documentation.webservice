﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.DAL.Model
{
    public class FunctionName
    {
        public Guid Id { get; set; }

        public string SystemName { get; set; } = null!;

        public Guid TitleResourceId { get; set; }

        //public Guid DescriptionResourceId { get; set; }

        public virtual TextResource TitleResource { get; set; } = null!;

        //public TextResource DescriptionResource { get; set; } = null!;

        public virtual List<Function> Functions { get; set; } = null!;

        public virtual List<FunctionGroup> Groups { get; set; } = null!;
    }
}
