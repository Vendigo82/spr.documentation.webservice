﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.DAL.Model
{
    public class Project
    {
        public Guid Id { get; set; }

        public string SystemName { get; set; } = null!;

        public Guid TitleResourceId { get; set; }

        public virtual TextResource TitleResource { get; set; } = null!;

        public virtual List<ProjectVersion> Versions { get; set; } = null!;
    }
}
