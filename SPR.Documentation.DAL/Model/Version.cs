﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.DAL.Model
{
    /// <summary>
    /// Evaluate expression version
    /// </summary>
    public class Version
    {
        public Guid Id { get; set; }

        public int[] VersionValue { get; set; } = null!;

        public DateTimeOffset CreatedAt { get; set; }

        public virtual List<FunctionVersionAssignment> FormulaVersionAssignments { get; set; } = null!;
    }
}
