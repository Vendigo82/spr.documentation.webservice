﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace SPR.Documentation.DAL.Model
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum DataTypeEnum
    {
        Any, Integer, Float, String, Bool, DateTime, Array, Object
    }
}
