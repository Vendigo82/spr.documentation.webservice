﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.DAL.Model
{
    public class ProjectVersion
    {
        public Guid Id { get; set; }

        public Guid ProjectId { get; set; }

        public int[] Version { get; set; } = null!;

        public int[] LibraryVersion { get; set; } = null!;

        public virtual Project Project { get; set; } = null!;
    }
}
