﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.DAL.Model
{
    public class TextResourceRow
    {
        public Guid TextResourceId { get; set; }

        public Guid LanguageId { get; set; }

        public string Text { get; set; } = null!;

        public virtual Language Language { get; set; } = null!;

        public virtual TextResource TextResource { get; set; } = null!;
    }
}
