﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.DAL.Model
{
    public class Language
    {
        public Guid Id { get; set; }

        public string Code { get; set; } = null!;

        public string Title { get; set; } = null!;

        public virtual List<TextResourceRow> TextRows { get; set; } = null!;
    }
}
