﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.DAL.Model
{
    public class FunctionVersionAssignment
    {
        public Guid FunctionId { get; set; }

        public Guid VersionId { get; set; }

        public virtual Function Function { get; set; } = null!;

        public virtual Version Version { get; set; } = null!;
    }
}
