﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.DAL.Model
{
    public class FunctionExample
    {
        public Guid Id { get; set; }

        public Guid FunctionId { get; set; }

        public string Example { get; set; } = null!;

        public int Order { get; set; }

        public virtual Function Function { get; set; } = null!;
    }
}
