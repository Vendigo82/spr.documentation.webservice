﻿using System;
using System.Collections.Generic;

namespace SPR.Documentation.DAL.Model
{
    public class FunctionGroup
    {
        public Guid Id { get; set; }

        public string Alias { get; set; } = null!;

        public Guid TitleResourceId { get; set; }

        public virtual TextResource TitleResource { get; set; } = null!;

        public virtual List<FunctionName> Functions { get; set; } = null!;
    }
}
