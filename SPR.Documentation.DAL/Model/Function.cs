﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.DAL.Model
{
    public class Function
    {
        public Guid Id { get; set; }

        public Guid FunctionNameId { get; set; }

        public short ArgsCount { get; set; }

        public FunctionArgument[] Arguments { get; set; } = null!;

        public FunctionReturnValue? Return { get; set; }

        public Guid DescriptionResourceId { get; set; }

        public virtual FunctionName FunctionName { get; set; } = null!;

        public virtual TextResource DescriptionResource { get; set; } = null!;

        public virtual List<FunctionExample> Examples { get; set; } = null!;

        public virtual FunctionVersionAssignment? VersionAssignment { get; set; }
    }
}
