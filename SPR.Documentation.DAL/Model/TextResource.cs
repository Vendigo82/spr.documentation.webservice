﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.DAL.Model
{
    public class TextResource
    {
        public Guid Id { get; set; }

        public virtual List<TextResourceRow> Rows { get; set; } = null!;
    }
}
