﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.DAL.Model
{
    public class FunctionArgument
    {
        [System.Text.Json.Serialization.JsonPropertyName("type")]
        public DataTypeEnum Type { get; set; }

        [System.Text.Json.Serialization.JsonPropertyName("name")]
        public string Name { get; set; } = null!;
    }
}
