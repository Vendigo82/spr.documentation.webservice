﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPR.Documentation.DAL.Model;

namespace SPR.Documentation.DAL
{
    public class DocContext : DbContext
    {
        public const string FunctionUniqueKey = "unq_function_args_count_function_name_id";
        public const string GroupUniqueKey = "unq_function_group_alias";
        public const string ProjectUniqueKey = "unq_project_system_name";
        public const string ProjectVersionUniqueKey = "unq_project_version_project_id_version";

        public DocContext(DbContextOptions options)
            : base(options)
        {
        }


        public DbSet<Language> Languages { get; set; } = null!;
        public DbSet<TextResource> TextResources { get; set; } = null!;
        public DbSet<TextResourceRow> TextResourceRows { get; set; } = null!;

        public DbSet<Model.Version> Versions { get; set; } = null!;
        public DbSet<FunctionGroup> FunctionGroups { get; set; } = null!;
        public DbSet<FunctionName> FunctionNames { get; set; } = null!;
        public DbSet<Function> Functions { get; set; } = null!;
        public DbSet<FunctionExample> FunctionExamples { get; set; } = null!;

        public DbSet<ProjectVersion> ProjectVersions { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            //if (!optionsBuilder.IsConfigured)
            optionsBuilder.UseSnakeCaseNamingConvention();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.HasPostgresExtension("pgcrypto")
                .HasAnnotation("Relational:Collation", "English_United States.1252");

            modelBuilder.Entity<Language>(entity => {
                entity
                    .ToTable("language", "text")
                    .HasKey(p => p.Id);

                entity.HasIndex(p => p.Code).IsUnique();

                entity.Property(p => p.Id).HasDefaultValueSql("gen_random_uuid()");                
            });

            modelBuilder.Entity<TextResource>(entity => {
                entity.ToTable("text_resource", "text").HasKey(p => p.Id);
                entity.Property(p => p.Id).HasDefaultValueSql("gen_random_uuid()");
            });

            modelBuilder.Entity<TextResourceRow>(entity => {
                entity.ToTable("text_resource_row", "text").HasKey(p => new { p.LanguageId, p.TextResourceId });
                entity.HasOne(p => p.Language).WithMany(p => p.TextRows).OnDelete(DeleteBehavior.NoAction);
                entity.HasOne(p => p.TextResource).WithMany(p => p.Rows);
            });

            modelBuilder.Entity<Model.Version>(entity => {
                entity.ToTable("version", "doc").HasKey(p => p.Id);

                entity.HasIndex(p => p.VersionValue).IsUnique();

                entity.Property(p => p.Id).HasDefaultValueSql("gen_random_uuid()");
                entity.Property(p => p.VersionValue).IsRequired();
                entity.Property(p => p.CreatedAt).HasDefaultValueSql("timezone('UTC'::text, now())");                
            });

            modelBuilder.Entity<FunctionGroup>(entity => {
                entity.ToTable("function_group", "doc").HasKey(p => p.Id);
                entity.Property(p => p.Id).HasDefaultValueSql("gen_random_uuid()");
                entity.Property(p => p.Alias).IsRequired();
                entity.HasOne(p => p.TitleResource).WithMany().OnDelete(DeleteBehavior.NoAction);

                entity.HasIndex(p => p.Alias).IsUnique().HasDatabaseName(GroupUniqueKey);
            });

            modelBuilder.Entity<FunctionName>(entity => {
                entity.ToTable("function_name", "doc").HasKey(p => p.Id);
                entity.HasIndex(p => p.SystemName).IsUnique();

                entity.Property(p => p.Id).HasDefaultValueSql("gen_random_uuid()");
                entity.HasOne(p => p.TitleResource).WithMany().OnDelete(DeleteBehavior.NoAction);
                //entity.HasOne(p => p.DescriptionResource).WithMany().OnDelete(DeleteBehavior.NoAction);
                entity.HasMany(p => p.Groups).WithMany(p => p.Functions);
            });

            modelBuilder.Entity<Function>(entity => {
                entity.ToTable("function", "doc").HasKey(p => p.Id);
                entity.HasOne(p => p.FunctionName).WithMany(p => p.Functions);
                entity.HasOne(p => p.DescriptionResource).WithMany().OnDelete(DeleteBehavior.NoAction);
                //entity.HasOne(p => p.Formul)
                entity.Property(p => p.Id).HasDefaultValueSql("gen_random_uuid()");
                entity.Property(p => p.Arguments).HasColumnType("jsonb");
                entity.Property(p => p.Return).HasColumnType("jsonb");

                entity.HasIndex(p => new { p.ArgsCount, p.FunctionNameId }).IsUnique().HasDatabaseName(FunctionUniqueKey);
            });

            modelBuilder.Entity<FunctionExample>(entity => {
                entity.ToTable("example", "doc").HasKey(p => p.Id);
                entity.HasOne(p => p.Function).WithMany(p => p.Examples);
                entity.Property(p => p.Id).HasDefaultValueSql("gen_random_uuid()");
            });

            modelBuilder.Entity<FunctionVersionAssignment>(entity => {
                entity.ToTable("function_version_assignment", "doc").HasKey(p => new { p.FunctionId, p.VersionId });
                entity.HasOne(p => p.Function).WithOne(p => p.VersionAssignment!);
                entity.HasOne(p => p.Version);
            });

            modelBuilder.Entity<Project>(entity => {
                entity.ToTable("project", "project").HasKey(p => p.Id);
                entity.HasIndex(p => p.SystemName).IsUnique().HasDatabaseName(ProjectUniqueKey);
                entity.HasOne(p => p.TitleResource).WithMany().OnDelete(DeleteBehavior.NoAction);
                entity.Property(p => p.Id).HasDefaultValueSql("gen_random_uuid()");
            });

            modelBuilder.Entity<ProjectVersion>(entity => {
                entity.ToTable("project_version", "project").HasKey(p => p.Id);
                entity.HasIndex(p => new { p.Version, p.ProjectId }).IsUnique().HasDatabaseName(ProjectVersionUniqueKey);
                entity.HasOne(p => p.Project).WithMany(p => p.Versions);
                entity.Property(p => p.Id).HasDefaultValueSql("gen_random_uuid()");
            });
        }
    }
}
