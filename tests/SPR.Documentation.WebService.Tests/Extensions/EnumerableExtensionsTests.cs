﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.WebService.Extensions
{
    public class EnumerableExtensionsTests
    {
        [Theory, MemberData(nameof(IndexOfTestCases))]
        public void IndexOfTest(IEnumerable<Guid> sequence, Guid element, int expectedResult)
        {
            // arrange

            // action
            var result = sequence.IndexOf(element);

            // asserts
            result.Should().Be(expectedResult);
        }

        public static IEnumerable<object[]> IndexOfTestCases()
        {
            var seq = Enumerable.Range(1, 3).Select(i => Guid.NewGuid()).ToList();
            yield return new object[] { seq, seq[0], 0 };
            yield return new object[] { seq, seq[1], 1 };
            yield return new object[] { seq, seq[2], 2 };
            yield return new object[] { seq, Guid.NewGuid(), -1 };
        }        
    }
}
