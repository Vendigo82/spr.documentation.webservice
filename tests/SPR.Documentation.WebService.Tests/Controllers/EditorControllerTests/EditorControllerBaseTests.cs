﻿using Microsoft.Extensions.Options;
using Moq;
using SPR.Documentation.Services.Abstractions;
using SPR.Documentation.WebService.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.WebService.Tests.Controllers.EditorControllerTests
{
    public class EditorControllerBaseTests
    {
        protected readonly MockRepository mockRepository = new(MockBehavior.Default);
        protected readonly Mock<IFunctionsEditorService> serviceMock;
        protected readonly Mock<ITokenConvertor<DataContract.Editor.FunctionBriefModel>> tokenMock;
        protected readonly Mock<IOptions<Settings.EditorControllerSettings>> optionsMock = new();
        protected readonly EditorFunctionsController target;

        public EditorControllerBaseTests()
        {
            optionsMock.Setup(f => f.Value).Returns(new Settings.EditorControllerSettings());
            serviceMock = mockRepository.Create<IFunctionsEditorService>();
            tokenMock = mockRepository.Create<ITokenConvertor<DataContract.Editor.FunctionBriefModel>>();
            target = new EditorFunctionsController(serviceMock.Object, tokenMock.Object, optionsMock.Object);
        }
    }
}
