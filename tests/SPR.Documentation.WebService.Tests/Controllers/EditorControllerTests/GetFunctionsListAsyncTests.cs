﻿using AutoFixture.Xunit2;
using FluentAssertions;
using FluentAssertions.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SPR.Documentation.DataContract.Editor;
using SPR.Documentation.Services.Exceptions;
using SPR.Documentation.Services.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.WebService.Tests.Controllers.EditorControllerTests
{
    public class GetFunctionsListAsyncTests : EditorControllerBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessRequestWithoutTokenTest(IEnumerable<FunctionBriefModel> data, string token)
        {
            // setup
            serviceMock.Setup(f => f.GetFunctionsListAsync(It.IsAny<FunctionsRequestOptions>())).ReturnsAsync(data);
            tokenMock.Setup(f => f.Serialize(data.Last())).Returns(token);

            // action
            var result = await target.GetFunctionsListAsync();

            // asserts
            result.Should().BeOkObjectResult().WithValueEquivalentTo(new DataContract.PartialItemsContainer<FunctionBriefModel> {
                Items = data,
                NextToken = token
            }, o => o.Excluding(p => p.Exhausted));

            serviceMock.Verify(f => f.GetFunctionsListAsync(Match.Create<FunctionsRequestOptions>(i => i.StartsFrom == null)), Times.Once);
            tokenMock.Verify(f => f.Serialize(data.Last()), Times.Once);
            mockRepository.VerifyNoOtherCalls();
        }

        [Theory, AutoData]
        public async Task SuccessRequestWithTokenTest(IEnumerable<FunctionBriefModel> data, string token, string requestToken, FunctionBriefModel startsFrom)
        {
            // setup
            serviceMock.Setup(f => f.GetFunctionsListAsync(It.IsAny<FunctionsRequestOptions>())).ReturnsAsync(data);
            tokenMock.Setup(f => f.Serialize(data.Last())).Returns(token);
            tokenMock.Setup(f => f.Deserialize(requestToken)).Returns(startsFrom);

            // action
            var result = await target.GetFunctionsListAsync(token: requestToken);

            // asserts
            result.Should().BeOkObjectResult().WithValueEquivalentTo(new DataContract.PartialItemsContainer<FunctionBriefModel> {
                Items = data,
                NextToken = token
            }, o => o.Excluding(p => p.Exhausted));

            tokenMock.Verify(f => f.Deserialize(requestToken), Times.Once);
            serviceMock.Verify(f => f.GetFunctionsListAsync(Match.Create<FunctionsRequestOptions>(i => i.StartsFrom == startsFrom)), Times.Once);
            tokenMock.Verify(f => f.Serialize(data.Last()), Times.Once);
            mockRepository.VerifyNoOtherCalls();
        }

        [Theory, AutoData]
        public async Task FilterTest(IEnumerable<FunctionBriefModel> data, string filter)
        {
            // setup
            serviceMock.Setup(f => f.GetFunctionsListAsync(It.IsAny<FunctionsRequestOptions>())).ReturnsAsync(data);

            // action
            var result = await target.GetFunctionsListAsync(filter: filter);

            // asserts
            result.Should().BeOkObjectResult().WithValueEquivalentTo(new DataContract.PartialItemsContainer<FunctionBriefModel> {
                Items = data
            }, o => o.Excluding(p => p.Exhausted));

            serviceMock.Verify(f => f.GetFunctionsListAsync(Match.Create<FunctionsRequestOptions>(i => i.Filter == filter)), Times.Once);
            serviceMock.VerifyNoOtherCalls();
        }

        [Theory]
        [InlineAutoData(null, 5, 5)]
        [InlineAutoData(3, 5, 3)]
        [InlineAutoData(6, 5, 5)]
        public async Task MaxCountTest(int? requestMaxCount, int optionsMaxCount, int expectedMaxCount, IEnumerable<FunctionBriefModel> data)
        {
            // setup
            serviceMock.Setup(f => f.GetFunctionsListAsync(It.IsAny<FunctionsRequestOptions>())).ReturnsAsync(data);
            optionsMock.Setup(f => f.Value).Returns(new Settings.EditorControllerSettings { MaxFunctionsCount = optionsMaxCount });

            // action
            var result = await target.GetFunctionsListAsync(maxCount: requestMaxCount);

            // asserts
            result.Should().BeOkObjectResult().WithValueEquivalentTo(new DataContract.PartialItemsContainer<FunctionBriefModel> {
                Items = data,
            }, o => o.Excluding(p => p.Exhausted));

            serviceMock.Verify(f => f.GetFunctionsListAsync(Match.Create<FunctionsRequestOptions>(i => i.MaxCount == expectedMaxCount)), Times.Once);
            serviceMock.VerifyNoOtherCalls();
        }

        [Theory, AutoData]
        public async Task InvalidTokenException_ShouldBadRequest_Test(InvalidTokenException e, string requestToken) 
        {
            // setup
            tokenMock.Setup(f => f.Deserialize(requestToken)).Throws(e);

            // action
            var result = await target.GetFunctionsListAsync(token: requestToken);

            // asserts
            result.Should()
                .BeBadRequestObjectResult()
                .Error.Should().BeOfType<ValidationProblemDetails>().Which
                .Errors.Should().ContainKey("token")
                .WhoseValue.Should().ContainSingle()
                .Which.Should().Be(e.Message);
        }

        [Theory]
        [InlineAutoData(5, true)]
        [InlineAutoData(2, false)]
        public async Task ExhaustedTest(int maxCount, bool expectedExhausted, IEnumerable<FunctionBriefModel> data)
        {
            // setup
            serviceMock.Setup(f => f.GetFunctionsListAsync(It.IsAny<FunctionsRequestOptions>())).ReturnsAsync(data);
            tokenMock.Setup(f => f.Serialize(data.Last())).Returns("");

            // action
            var result = await target.GetFunctionsListAsync(maxCount: maxCount);

            // asserts
            result.Should().BeOkObjectResult().WithValueEquivalentTo(new DataContract.PartialItemsContainer<FunctionBriefModel> {
                Items = data,
                Exhausted = expectedExhausted
            }, o => o.Excluding(p => p.NextToken));
        }
    }
}
