﻿using AutoFixture.Xunit2;
using FluentAssertions;
using FluentAssertions.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SPR.Documentation.DataContract.Editor;
using SPR.Documentation.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.WebService.Tests.Controllers.EditorControllerTests
{
    public class UpdateFunctionAsyncTests : EditorControllerBaseTests
    {
        [Theory, AutoData]
        public async Task UpdateFunction_ShouldBeSuccess(FunctionModel model)
        {
            // setup
            //serviceMock.Setup(f => f.UpdateFunctionAsync(model)).ReturnsAsync(model.Id);

            // action
            var result = await target.UpdateFunctionAsync(model.Id, model);

            // asserts
            result.Should().BeOkResult();

            serviceMock.Verify(f => f.UpdateFunctionAsync(model), Times.Once);
        }

        [Theory]
        [InlineAutoData(0)]
        [InlineAutoData(1)]
        [InlineAutoData(2)]
        public async Task UpdateFunction_ThrowsGroupNotFoundException_ShouldBeUnprocessableEntity(int index, FunctionModel model)
        {
            // setup
            serviceMock.Setup(f => f.UpdateFunctionAsync(model)).ThrowsAsync(new GroupNotFoundException(model.Groups.Skip(index).Take(1)));

            // action
            var result = await target.UpdateFunctionAsync(model.Id, model);

            // asserts
            var value = result.Should().BeObjectResult().WithStatusCode(StatusCodes.Status422UnprocessableEntity).Value;
            value.Should().BeOfType<ValidationProblemDetails>().Which.Errors.Should().ContainKey($"$.Groups[{index}]");
        }
    }
}
