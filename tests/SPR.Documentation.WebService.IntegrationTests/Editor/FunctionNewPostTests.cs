﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Linq;
using SPR.Documentation.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace SPR.Documentation.WebService.IntegrationTests.Editor
{
    public class FunctionNewPostTests : BaseTests
    {
        public FunctionNewPostTests(WebFactoryBase fixture, ITestOutputHelper testOutput) : base(fixture, testOutput)
        {
        }

        [Theory, AutoData]
        public async Task NewFunctionShouldBeAdded(Guid functionId, string functionName)
        {
            // setup
            var model = new {
                id = functionId,
                systemName = functionName,
                argsCount = 1,
                arguments = new[] {
                    new {
                        type = "String",
                        name = "arg1"
                    }
                },
                @return = new {
                    type = "String",
                },
                titles = new[] { new { lang = "ru", text = "ru-title" } },
                descriptions = new[] { new { lang = "ru", text = "ru-descr" } },
                libraryVersion = "1.0.0"
            };

            // action
            var response = await client.PostAsJsonAsync("editor/functions", model);

            // asserts
            response.Should().Be201Created().And.HaveHeader("Location");
            response.Headers.Location.LocalPath.Should().Be($"/editor/functions/{functionId}");

            var dbitem = context.Functions.FirstOrDefault(i => i.Id == functionId);
            dbitem.FunctionName.SystemName.Should().Be(functionName);
            dbitem.Should().BeEquivalentTo(new {
                ArgsCount = model.argsCount,
                Arguments = model.arguments.Select(i => new { Type = Enum.Parse<DataTypeEnum>(i.type), Name = i.name }),
                Return = new { Type = Enum.Parse<DataTypeEnum>(model.@return.type) },
                VersionAssignment = new { Version = new { VersionValue = new[] { 1, 0, 0 } } }
            });

            dbitem.FunctionName.TitleResource.Rows.Select(i => new { lang = i.Language.Code, text = i.Text }).Should().BeEquivalentTo(model.titles);
            dbitem.DescriptionResource.Rows.Select(i => new { lang = i.Language.Code, text = i.Text }).Should().BeEquivalentTo(model.descriptions);
            dbitem.FunctionName.Groups.Should().BeEmpty();
        }

        [Theory, AutoData]
        public async Task InsertFunctionWithGroupsTest(Guid functionId, string functionName)
        {
            // setup
            var dbGroup = context.FunctionGroups.First();

            var model = new {
                id = functionId,
                systemName = functionName,
                argsCount = 0,
                arguments = Array.Empty<object>(),
                @return = new {
                    type = "String",
                },
                titles = new[] { new { lang = "ru", text = "ru-title" } },
                descriptions = new[] { new { lang = "ru", text = "ru-descr" } },
                libraryVersion = "1.0.0",
                groups = new[] { dbGroup.Id.ToString() }
            };

            // action
            var response = await client.PutAsJsonAsync("editor/functions", model);

            // asserts
            response.Should().Be201Created();

            var dbitem = context.Functions.FirstOrDefault(i => i.Id == functionId);
            dbitem.FunctionName.Groups.Should().ContainSingle().Which.Id.Should().Be(dbGroup.Id);
        }

        [Theory, AutoData]
        public async Task UnknownLanguageCode(Guid functionId, string functionName)
        {
            // setup
            var model = new {
                id = functionId,
                systemName = functionName,
                argsCount = 0,
                arguments = Array.Empty<object>(),
                @return = new {
                    type = "String",
                },
                titles = new[] { new { lang = "xyz", text = "ru-title" } },
                descriptions = new[] { new { lang = "ru", text = "ru-descr" } },
                libraryVersion = "1.0.0"
            };

            // action
            var response = await client.PostAsJsonAsync("editor/functions", model);

            // asserts
            response.Should().HaveHttpStatusCode(System.Net.HttpStatusCode.UnprocessableEntity).And.BeAs(new { language = "xyz" });
        }

        [Theory, AutoData]
        public async Task UnknownVersion(Guid functionId, string functionName)
        {
            // setup
            var model = new {
                id = functionId,
                systemName = functionName,
                argsCount = 0,
                arguments = Array.Empty<object>(),
                @return = new {
                    type = "String",
                },
                titles = new[] { new { lang = "ru", text = "ru-title" } },
                descriptions = new[] { new { lang = "ru", text = "ru-descr" } },
                libraryVersion = "0.0.0"
            };

            // action
            var response = await client.PostAsJsonAsync("editor/functions", model);

            // asserts            
            response.Should().HaveHttpStatusCode(System.Net.HttpStatusCode.UnprocessableEntity);
        }

        [Theory, AutoData]
        public async Task TryToInsertAlreadyExistedFunctionShouldBeConflict(Guid functionId)
        {
            // setup
            var model = new {
                id = functionId,
                systemName = "test_function1",
                argsCount = 0,
                arguments = Array.Empty<object>(),
                @return = new {
                    type = "String",
                },
                titles = new[] { new { lang = "ru", text = "ru-title" } },
                descriptions = new[] { new { lang = "ru", text = "ru-descr" } },
                libraryVersion = (string)null
            };

            // action
            var response = await client.PostAsJsonAsync("editor/functions", model);

            // asserts            
            response.Should().Be409Conflict().And.BeAs(new { Title = "Function already exists" });
        }

        [Theory, AutoData]
        public async Task InsertFunctionWithUnknownGroup_ShouldBeUnprocessableEntity(Guid functionId, string functionName)
        {
            // setup
            var model = new {
                id = functionId,
                systemName = functionName,
                argsCount = 0,
                arguments = Array.Empty<object>(),
                @return = new {
                    type = "String",
                },
                titles = new[] { new { lang = "ru", text = "ru-title" } },
                descriptions = new[] { new { lang = "ru", text = "ru-descr" } },
                libraryVersion = "1.0.0",
                groups = new[] { Guid.NewGuid().ToString() }
            };

            // action
            var response = await client.PutAsJsonAsync("editor/functions", model);
            var body = await response.Content.ReadAsStringAsync();
            testOutput.WriteLine(body);

            // asserts
            response.Should().HaveHttpStatusCode(System.Net.HttpStatusCode.UnprocessableEntity);
        }
    }
}
