﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.WebService.IntegrationTests.Editor
{
    public class FunctionGetTests : BaseTests
    {
        public FunctionGetTests(WebFactoryBase fixture) : base(fixture)
        {
        }

        [Fact]
        public async Task SuccessGetTest()
        {
            // setup
            using var scope = fixture.Services.CreateScope();

            var dbitem = await context.Functions
                .FirstAsync(c => c.FunctionName.SystemName == "test_function2" && c.ArgsCount == 1);

            // action
            var response = await fixture.CreateClient().GetAsync($"editor/functions/{dbitem.Id}");

            // asserts
            response.Should().Be200Ok().And.BeAs(new { 
                id = dbitem.Id,
                systemName = dbitem.FunctionName.SystemName,
                argsCount = dbitem.ArgsCount,
                arguments = new[] { 
                    new { 
                        type = "String",
                        name = "arg1"
                    }
                },
                @return = new {
                    type = "String",                    
                },
                titles = dbitem.FunctionName.TitleResource.Rows.Select(i => new { Lang = i.Language.Code, i.Text }),
                descriptions = dbitem.DescriptionResource.Rows.Select(i => new { Lang = i.Language.Code, i.Text }),
                libraryVersion = "1.0.0"
            });
        }

        [Theory, AutoData]
        public async Task FunctionNotFound_ShouldBeNoContent_Test(Guid functionId) 
        {
            // setup

            // action
            var response = await fixture.CreateClient().GetAsync($"editor/functions/{functionId}");

            // asserts
            response.Should().Be204NoContent();
        }

        [Theory]
        [InlineData("test_function1", 0, "group1")]
        [InlineData("test_function2", 1, "group2")]
        [InlineData("test_function2", 2, "group2")]
        public async Task GetFunction_GroupsFieldShouldHasValues(string functionName, int argsCount, string expectedGroupName)
        {
            // setup
            var expectedGroupId = context.FunctionGroups.Single(i => i.Alias == expectedGroupName);
            var dbitem = await context.Functions
                .FirstAsync(c => c.FunctionName.SystemName == functionName && c.ArgsCount == argsCount);

            // action
            var response = await fixture.CreateClient().GetAsync($"editor/functions/{dbitem.Id}");

            // asserts
            response.Should().Be200Ok().And.BeAs(new { groups = new[] { expectedGroupId.Id } });
        }
    }
}
