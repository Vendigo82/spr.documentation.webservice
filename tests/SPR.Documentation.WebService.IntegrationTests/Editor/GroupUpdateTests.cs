﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.WebService.IntegrationTests.Editor
{
    public class GroupUpdateTests : BaseTests
    {
        private Guid? groupIdToDelete = null;

        public GroupUpdateTests(WebFactoryBase fixture) : base(fixture)
        {
        }

        [Theory, AutoData]
        public async Task UpdateExistedTest_ShouldBeSuccess(Guid id, string oldAlias, string newAlias)
        {
            // setup
            var dbitem = new DAL.Model.FunctionGroup {
                Id = id,
                Alias = oldAlias,
                TitleResource = new DAL.Model.TextResource { 
                    Rows = new List<DAL.Model.TextResourceRow> { 
                        new DAL.Model.TextResourceRow { LanguageId = context.Languages.First(i => i.Code == "ru").Id, Text = "old-title-ru" }
                    }
                }
            };
            await context.FunctionGroups.AddAsync(dbitem);
            await context.SaveChangesAsync();

            groupIdToDelete = dbitem.Id;

            var model = new {
                id,
                alias = newAlias,
                titles = new[] { new { lang = "ru", text = "new-title-ru" } }
            };

            // action
            var response = await client.PostAsJsonAsync($"editor/groups/{id}", model);

            // asserts
            response.Should().Be200Ok();

            context.ChangeTracker.Clear();
            dbitem = await context.FunctionGroups.FirstAsync(i => i.Id == id);

            dbitem.Alias.Should().Be(newAlias);
            dbitem.TitleResource.Rows.Select(i => new { lang = i.Language.Code, text = i.Text }).Should().BeEquivalentTo(model.titles);
        }

        [Theory, AutoData]
        public async Task TryToUpdateGroupAliasToAlreadyExisted_ShouldBeConflict(Guid id, string oldAlias)
        {
            // setup
            var dbitem = new DAL.Model.FunctionGroup {
                Id = id,
                Alias = oldAlias,
                TitleResource = new DAL.Model.TextResource {
                    Rows = new List<DAL.Model.TextResourceRow> {
                        new DAL.Model.TextResourceRow { LanguageId = context.Languages.First(i => i.Code == "ru").Id, Text = "old-title-ru" }
                    }
                }
            };
            await context.FunctionGroups.AddAsync(dbitem);
            await context.SaveChangesAsync();

            groupIdToDelete = dbitem.Id;

            var model = new {
                id,
                alias = "group1",
                titles = new[] { new { lang = "ru", text = "new-title-ru" } }
            };

            // action
            var response = await client.PostAsJsonAsync($"editor/groups/{id}", model);

            // asserts
            response.Should().Be409Conflict().And.BeAs(new { Title = "Group already exists" });
        }

        [Theory, AutoData]
        public async Task TryToUpdateUnknownGroup_ShouldBeNotFound(Guid id, string alias)
        {
            // setup
            var model = new {
                id,
                alias,
                titles = new[] { new { lang = "ru", text = "new-title-ru" } }
            };

            // action
            var response = await client.PostAsJsonAsync($"editor/groups/{id}", model);

            // asserts
            response.Should().Be404NotFound().And.BeAs(new { Title = "Group does not exist" });
        }

        public override async ValueTask DisposeAsync()
        {
            if (groupIdToDelete != null)
            {
                var dbitem = await context.FunctionGroups.FirstOrDefaultAsync(i => i.Id == groupIdToDelete.Value);
                if (dbitem != null)
                {
                    context.Entry(dbitem.TitleResource).State = EntityState.Deleted;
                    context.Entry(dbitem).State = EntityState.Deleted;
                    await context.SaveChangesAsync();
                }
            }
            await base.DisposeAsync();
        }
    }
}
