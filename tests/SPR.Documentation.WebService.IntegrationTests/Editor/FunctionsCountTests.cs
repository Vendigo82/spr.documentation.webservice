﻿using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.WebService.IntegrationTests.Editor
{
    public class FunctionsCountTests : BaseTests
    {
        public FunctionsCountTests(WebFactoryBase fixture) : base(fixture)
        {
        }

        [Fact]
        public async Task CountWithoutFilterTest()
        {
            // setup


            // action
            var response = await client.GetAsync("editor/functions/count");

            // asserts            
            var expectedCount = await context.Functions.CountAsync();
            response.Should().Be200Ok().And.BeAs(new { count = expectedCount });
        }

        [Theory]
        [InlineData("function1")]
        public async Task CountWithFilterTest(string filter)
        {
            // setup


            // action
            var response = await client.GetAsync($"editor/functions/count?filter={filter}");

            // asserts            
            var expectedCount = await context.Functions.Where(i => i.FunctionName.SystemName.Contains(filter)).CountAsync();
            response.Should().Be200Ok().And.BeAs(new { count = expectedCount });
        }
    }
}
