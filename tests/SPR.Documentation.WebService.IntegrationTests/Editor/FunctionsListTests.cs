using FluentAssertions;
using FluentAssertions.Json;
using FluentAssertions.Web;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.WebService.IntegrationTests.Editor
{
    public class FunctionsListTests : BaseTests
    {

        public FunctionsListTests(WebFactoryBase fixture) : base(fixture)
        {            
        }

        [Fact]
        public async Task GetListTest()
        {
            // setup

            // action
            var response = await client.GetAsync("editor/functions");

            // asserts
            response.Should().Be200Ok();
            var jtoken = JObject.Parse(await response.Content.ReadAsStringAsync());
            
            var items = jtoken.Should().HaveElement("items").Which;
            items.Should().BeAssignableTo<IEnumerable<JToken>>().Which.Should().NotBeEmpty();
            var item = items.First;
            item.Should().HaveElement("systemName").And.HaveElement("argsCount").And.HaveElement("id");
        }


        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(2)]
        public async Task GetListWithOffsetTest(int offset)
        {
            // setup
            var expectedFirstId = context.Functions.OrderBy(i => i.FunctionName.SystemName).ThenBy(i => i.ArgsCount).Skip(offset).First().Id;

            // action
            var response = await client.GetAsync($"editor/functions?offset={offset}&maxCount=1");

            // asserts
            response.Should().Be200Ok();            

            var ids = await GetIdsAsync(response);
            ids.Should().NotBeNullOrEmpty().And.Subject.First().Should().Be(expectedFirstId);            
        }

        [Theory]
        [InlineData("function1")]
        [InlineData("function2")]
        public async Task FilterTest(string filter)
        {
            // setup

            // action
            var response = await client.GetAsync($"editor/functions?filter={filter}");

            // asserts
            response.Should().Be200Ok();
            var jtoken = JToken.Parse(await response.Content.ReadAsStringAsync());

            var items = jtoken.Should().HaveElement("items").Which;
            items.Should()
                .BeOfType<JArray>().Which
                .Select(i => (string)i["systemName"]).Should()
                .NotBeEmpty().And
                .OnlyContain(i => i.Contains(filter));
        }

        [Theory]
        [InlineData(2, 2)]
        [InlineData(1, 1)]
        public async Task MaxCountTest(int maxCount, int expectedCount)
        {
            // setup

            // action
            var response = await client.GetAsync($"editor/functions?maxCount={maxCount}");

            // asserts
            response.Should().Be200Ok();
            var jtoken = JToken.Parse(await response.Content.ReadAsStringAsync());

            jtoken.Should()
                .HaveElement("items").Which.Should()
                .BeAssignableTo<IEnumerable<JToken>>().Which.Should()
                .HaveCount(expectedCount);
        }

        [Fact]
        public async Task SequenceRequestsWithNextTokenTest()
        {
            // setup

            // action
            var responseTotal = await client.GetAsync($"editor/functions?maxCount=4");
            var responseFirst = await client.GetAsync($"editor/functions?maxCount=2");

            // asserts && setup 2
            responseFirst.Should().Be200Ok();
            var ids1 = await GetIdsAsync(responseFirst);
            var jtoken1 = JToken.Parse(await responseFirst.Content.ReadAsStringAsync());
            var nextToken = jtoken1.Should().HaveElement("nextToken").Which.Value<string>();

            // action 2
            var responseSecond = await client.GetAsync($"editor/functions?maxCount=2&token={nextToken}");

            // asserts 2
            responseSecond.Should().Be200Ok();

            var expectedIds = await GetIdsAsync(responseTotal);
            var ids2 = await GetIdsAsync(responseSecond);

            ids1.Concat(ids2).Should().Equal(expectedIds);            
        }

        private IEnumerable<Guid> GetIds(JToken jtoken)
        {
            return jtoken["items"].Should().BeAssignableTo<JArray>().Which.Select(i => (Guid)i["id"]);
        }

        private async Task<IEnumerable<Guid>> GetIdsAsync(HttpResponseMessage response)
        {
            var jtoken = JToken.Parse(await response.Content.ReadAsStringAsync());
            return GetIds(jtoken);
        }
    }
}
