﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.WebService.IntegrationTests.Editor
{
    public class GetDataTypesTests : BaseTests
    {
        public GetDataTypesTests(WebFactoryBase fixture) : base(fixture)
        {
        }  
        
        [Fact]
        public async Task ShouldBeSuccessAndNonEmptyListReturns()
        {
            // setup

            // action
            var response = await client.GetFromJsonAsync<ResponseModel>("editor/datatypes");

            // asserts
            response.Should().NotBeNull();
            response.Items.Should().NotBeNullOrEmpty();
        }

        public class ResponseModel
        {
            public IEnumerable<string> Items { get; set; }
        }
    }
}
