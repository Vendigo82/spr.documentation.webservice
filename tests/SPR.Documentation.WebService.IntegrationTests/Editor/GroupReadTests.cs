﻿using FluentAssertions;
using FluentAssertions.Json;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.WebService.IntegrationTests.Editor
{
    public class GroupReadTests : BaseTests
    {
        public GroupReadTests(WebFactoryBase fixture) : base(fixture)
        {
        }

        [Fact]
        public async Task GetGroupList_ResponseShouldContainAllGroups()
        {
            // setup

            // action
            var response = await client.GetAsync("editor/groups");

            // asserts
            response.Should().Be200Ok();
            var content = await response.Content.ReadAsStringAsync();
            var jo = JToken.Parse(content);

            var items = jo.Should().HaveElement("items").Which.Should().BeOfType<JArray>().Which;
            items.Count.Should().Be(await context.FunctionGroups.CountAsync());
            items.AsEnumerable().Should().OnlyContain(i => i.SelectToken("id") != null && i.SelectToken("alias") != null);
        }

        [Fact]
        public async Task GetGroupById_ShouldSuccess()
        {
            // setup
            var dbitem = await context.FunctionGroups.Where(i => i.Alias == "group1").SingleAsync();

            // action
            var response = await client.GetAsync($"editor/groups/{dbitem.Id}");

            // asserts
            response.Should().Be200Ok().And.BeAs(new {
                id = dbitem.Id,
                alias = dbitem.Alias,
                titles = dbitem.TitleResource.Rows.Select(i => new { lang = i.Language.Code, text = i.Text })
            });
        }

        [Fact]
        public async Task GetUnknownGroupById_ShouldBe204NoContent()
        {
            // setup

            // action
            var response = await client.GetAsync($"editor/groups/{Guid.NewGuid()}");

            // asserts
            response.Should().Be204NoContent();
        }
    }
}
