﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using SPR.Documentation.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.WebService.IntegrationTests.Editor
{
    public class FunctionsEditPostTests : BaseTests
    {
        public FunctionsEditPostTests(WebFactoryBase fixture) : base(fixture)
        {
        }

        [Theory, AutoData]
        public async Task FunctionShouldBeUpdated(Guid functionId, string functionName)
        {
            // setup
            var lang = await context.Languages.FirstAsync(i => i.Code == "ru");
            var dbitem = new DAL.Model.Function {
                Id = functionId,
                FunctionName = new FunctionName {
                    SystemName = functionName,
                    TitleResource = new TextResource {
                        Rows = new List<TextResourceRow> {
                        new TextResourceRow { LanguageId = lang.Id, Text = "ru-title-old" }
                    }
                    }
                },
                ArgsCount = 0,
                Arguments = Array.Empty<FunctionArgument>(),
                DescriptionResource = new TextResource {
                    Rows = new List<TextResourceRow> {
                    new TextResourceRow { LanguageId = lang.Id, Text = "ru-descr-old" }
                }
                },
                Return = new FunctionReturnValue { Type = DataTypeEnum.Integer },
            };
            context.Functions.Add(dbitem);
            await context.SaveChangesAsync();
            context.Entry(dbitem).State = EntityState.Detached;

            var model = new {
                id = functionId,
                systemName = functionName,
                argsCount = 1,
                arguments = new[] {
                    new {
                        type = "String",
                        name = "arg1"
                    }
                },
                @return = new {
                    type = "String",
                },
                titles = new[] { new { lang = "ru", text = "ru-title" } },
                descriptions = new[] { new { lang = "ru", text = "ru-descr" } },
                libraryVersion = "1.0.0"
            };

            // action
            var response = await client.PostAsJsonAsync($"editor/functions/{functionId}", model);

            // asserts
            response.Should().Be200Ok();

            context.ChangeTracker.Clear();

            dbitem = context.Functions.FirstOrDefault(i => i.Id == functionId);
            dbitem.FunctionName.SystemName.Should().Be(functionName);
            dbitem.Should().BeEquivalentTo(new {
                ArgsCount = model.argsCount,
                Arguments = model.arguments.Select(i => new { Type = Enum.Parse<DataTypeEnum>(i.type), Name = i.name }),
                Return = new { Type = Enum.Parse<DataTypeEnum>(model.@return.type) },
                VersionAssignment = new { Version = new { VersionValue = new[] { 1, 0, 0 } } }
            });

            dbitem.FunctionName.TitleResource.Rows.Select(i => new { lang = i.Language.Code, text = i.Text }).Should().BeEquivalentTo(model.titles);
            dbitem.DescriptionResource.Rows.Select(i => new { lang = i.Language.Code, text = i.Text }).Should().BeEquivalentTo(model.descriptions);
        }

        [Theory, AutoData]
        public async Task FunctionGroupShouldBeUpdated(Guid functionId, string functionName)
        {
            // setup
            var lang = await context.Languages.FirstAsync(i => i.Code == "ru");
            var groups = await context.FunctionGroups.Take(2).ToArrayAsync();

            var dbitem = new DAL.Model.Function {
                Id = functionId,
                FunctionName = new FunctionName {
                    SystemName = functionName,
                    TitleResource = new TextResource {
                        Rows = new List<TextResourceRow> {
                            new TextResourceRow { LanguageId = lang.Id, Text = "ru-title-old" }
                        }
                    },
                    Groups = new List<FunctionGroup> { groups[0] }
                },
                ArgsCount = 0,
                Arguments = Array.Empty<FunctionArgument>(),
                DescriptionResource = new TextResource {
                    Rows = new List<TextResourceRow> {
                    new TextResourceRow { LanguageId = lang.Id, Text = "ru-descr-old" }
                }
                },
                Return = new FunctionReturnValue { Type = DataTypeEnum.Integer },
            };
            context.Functions.Add(dbitem);
            await context.SaveChangesAsync();
            context.Entry(dbitem).State = EntityState.Detached;

            var model = new {
                id = functionId,
                systemName = functionName,
                argsCount = 1,
                arguments = new[] {
                    new {
                        type = "String",
                        name = "arg1"
                    }
                },
                @return = new {
                    type = "String",
                },
                titles = new[] { new { lang = "ru", text = "ru-title" } },
                descriptions = new[] { new { lang = "ru", text = "ru-descr" } },
                libraryVersion = "1.0.0",
                groups = new[] { groups[1].Id }
            };

            // action
            var response = await client.PostAsJsonAsync($"editor/functions/{functionId}", model);

            // asserts
            response.Should().Be200Ok();

            context.ChangeTracker.Clear();

            dbitem = context.Functions.FirstOrDefault(i => i.Id == functionId);
            dbitem.FunctionName.Groups.Should().ContainSingle().Which.Id.Should().Be(groups[1].Id);
        }

        [Theory, AutoData]
        public async Task EditUnknownFuntion_ShouldBeNotFound(Guid functionId)
        {
            // setup
            var model = new {
                id = functionId,
                systemName = "unknown",
                argsCount = 0,
                arguments = new object[] { },
                @return = new {
                    type = "String",
                },
                titles = new[] { new { lang = "ru", text = "ru-title" } },
                descriptions = new[] { new { lang = "ru", text = "ru-descr" } },
                libraryVersion = "1.0.0"
            };

            // action
            var response = await client.PostAsJsonAsync($"editor/functions/{functionId}", model);

            // asserts
            response.Should().Be404NotFound().And.BeAs(new { title = "Function does not exist"});
        }
    }
}
