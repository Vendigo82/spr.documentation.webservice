﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.WebService.IntegrationTests.Editor
{
    public class GroupCreateTests : BaseTests
    {
        private Guid? groupIdToDelete = null;

        public GroupCreateTests(WebFactoryBase fixture) : base(fixture)
        {
        }

        [Theory, AutoData]
        public async Task PutNewGroup_ShouldBeInserted_And_ResponseShouldBeBe201Created(Guid id, string alias)
        {
            // setup
            var model = new {
                id,
                alias,
                titles = new[] { new { lang = "ru", text = "ru-title" } },
            };
            groupIdToDelete = id;

            // action
            var response = await client.PutAsJsonAsync("editor/groups", model);

            // asserts
            response.Should().Be201Created().And.BeAs(new { id }).And
                .HaveHeader("Location");
            response.Headers.Location.LocalPath.Should().Be($"/editor/groups/{id}");

            var dbitem = await context.FunctionGroups.FirstAsync(i => i.Id == id);
            dbitem.Should().BeEquivalentTo(new { Id = id, Alias = alias });
            dbitem.TitleResource.Rows.Select(i => new { lang = i.Language.Code, text = i.Text }).Should()
                .NotBeEmpty().And
                .BeEquivalentTo(model.titles);
        }        

        [Fact]
        public async Task PutAlreadyExistedGroupId_ShouldBeConflict()
        {
            // setup
            var dbitem = await context.FunctionGroups.FirstAsync(i => i.Alias == "group1");

            var model = new {
                id = dbitem.Id,
                alias = Guid.NewGuid().ToString(),
                titles = new[] { new { lang = "ru", text = "ru-title" } },
            };

            // action
            var response = await client.PutAsJsonAsync("editor/groups", model);

            // asserts
            response.Should().Be409Conflict().And.BeAs(new { title = "Primary key conflict" });
        }

        [Fact]
        public async Task PutAlreadyExistedGroupAlias_ShouldBeConflict()
        {
            // setup
            var dbitem = await context.FunctionGroups.FirstAsync(i => i.Alias == "group1");

            var model = new {
                id = Guid.NewGuid(),
                alias = dbitem.Alias,
                titles = new[] { new { lang = "ru", text = "ru-title" } },
            };

            // action
            var response = await client.PutAsJsonAsync("editor/groups", model);

            // asserts
            response.Should().Be409Conflict().And.BeAs(new { title = "Group already exists" });
        }

        [Theory, AutoData]
        public async Task PutGroupWithUnknownLanguage_ShouldBeUnprocessableEntity(Guid id, string alias)
        {
            // setup
            var model = new {
                id,
                alias,
                titles = new[] { new { lang = "unknown", text = "ru-title" } },
            };
            groupIdToDelete = id;

            // action
            var response = await client.PutAsJsonAsync("editor/groups", model);

            // asserts
            response.Should().HaveHttpStatusCode(System.Net.HttpStatusCode.UnprocessableEntity).And.BeAs(new { language = "unknown" });
        }

        public override async ValueTask DisposeAsync()
        {
            if (groupIdToDelete != null)
            {
                var dbitem = await context.FunctionGroups.FirstOrDefaultAsync(i => i.Id == groupIdToDelete.Value);
                if (dbitem != null)
                {
                    context.Entry(dbitem.TitleResource).State = EntityState.Deleted;
                    context.Entry(dbitem).State = EntityState.Deleted;
                    await context.SaveChangesAsync();
                }
            }
            await base.DisposeAsync();
        }
    }
}
