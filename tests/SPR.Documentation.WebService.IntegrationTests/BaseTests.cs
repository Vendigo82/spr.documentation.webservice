﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SPR.Documentation.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

[assembly: CollectionBehavior(DisableTestParallelization = true)]

namespace SPR.Documentation.WebService.IntegrationTests
{
    public class BaseTests : IClassFixture<WebFactoryBase>, IAsyncDisposable
    {
        protected readonly ITestOutputHelper testOutput;
        protected readonly WebFactoryBase fixture;
        protected readonly HttpClient client;

        protected readonly DocContext context;

        public BaseTests(WebFactoryBase fixture, ITestOutputHelper testOutput)
        {
            this.fixture = fixture;
            this.testOutput = testOutput;
            client = this.fixture.CreateClient();

            var config = fixture.Services.GetRequiredService<IConfiguration>();

            var optBuilder = new DbContextOptionsBuilder<DocContext>()
                .UseNpgsql(config.GetConnectionString("Default"))
                .UseLazyLoadingProxies();
            //.ConfigureWarnings(w => w.Ignore(InMemoryEventId.TransactionIgnoredWarning));
            context = new DocContext(optBuilder.Options);

        }

        public BaseTests(WebFactoryBase fixture) : this(fixture, null)
        {
        }

        public virtual async ValueTask DisposeAsync()
        {
            await context.DisposeAsync();
            //GC.SuppressFinalize(this);
        }
    }
}
