﻿using FluentAssertions;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.WebService.IntegrationTests.Default
{
    public class InfoEndpoinsTests : BaseTests
    {
        public InfoEndpoinsTests(WebFactoryBase fixture) : base(fixture)
        {
        }
    
        [Fact]
        public async Task VersionTest()
        {
            // setup

            // action
            var response = await fixture.CreateClient().GetAsync("/version");

            // asserts
            response.Should().Be200Ok();
            var jo = JObject.Parse(await response.Content.ReadAsStringAsync());
            jo.Should().ContainKey("version").WhoseValue.Value<string>().Should().Match("*.*.*.*");
        }
    }
}
