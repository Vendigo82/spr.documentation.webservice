﻿using FluentAssertions;
using FluentAssertions.Json;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace SPR.Documentation.WebService.IntegrationTests.Doc
{
    public class GetFunctionsListTests : BaseTests
    {
        public GetFunctionsListTests(WebFactoryBase fixture, ITestOutputHelper testOutput) : base(fixture, testOutput)
        {
        }

        [Theory]
        [InlineData("ru", "project1", "1.0.0", "Заголовок test function 1")]
        [InlineData("en", "project1", "1.0.0", "Title test function 1")]
        [InlineData("en", "project2", "1.0.0", "Title test function 1")]
        [InlineData("en", "project1", "2.0.0", "Title test function 1")]
        [InlineData("en", "project1", "3.0.0", "Title test function 1")]
        [InlineData("hh", "project1", "1.0.0", "Title test function 1")]
        public async Task GetAllFunctions_ShouldSuccessTest(string lang, string project, string version, string expectedTitle)
        {
            // setup

            // action
            var response = await client.GetAsync($"doc/{project}/{version}/{lang}/functions");

            // asserts
            response.Should().Be200Ok();
            var contentString = await response.Content.ReadAsStringAsync();
            var jobject = JToken.Parse(contentString);
            var items = jobject.Should().HaveElement("items").Which.Should().BeOfType<JArray>().Which;
            var item = items.Where(i => (string)i["systemName"] == "test_function1").Should().ContainSingle().Which;
            item.Should().HaveElement("title").Which.Value<string>().Should().Be(expectedTitle);
        }

        [Theory]
        [InlineData("project1", "1.0.0", "test_function3", false)]
        [InlineData("project1", "2.0.0", "test_function3", true)]
        [InlineData("project1", "3.0.0", "test_function3", true)]
        [InlineData("project2", "1.0.0", "test_function3", false)]
        [InlineData("project2", "2.0.0", "test_function3", false)]
        public async Task GetAllFunctions_FilteredByProjectAndVersion(string project, string version, string functionName, bool expectedExists)
        {
            // setup

            // action
            var response = await client.GetAsync($"doc/{project}/{version}/en/functions");

            // asserts
            response.Should().Be200Ok();
            var contentString = await response.Content.ReadAsStringAsync();
            var jobject = JToken.Parse(contentString);
            var items = jobject.Should().HaveElement("items").Which.Should().BeOfType<JArray>().Which;
            if (expectedExists)
                items.Where(i => (string)i["systemName"] == functionName).Should().ContainSingle();
            else
                items.Where(i => (string)i["systemName"] == functionName).Should().BeEmpty();
        }

        [Theory]
        [InlineData("", "test_function1", true)]
        [InlineData("group1", "test_function1", true)]
        [InlineData("group2", "test_function1", false)]
        [InlineData("group1", "test_function2", false)]
        [InlineData("group2", "test_function2", true)]
        public async Task GetAllFunctions_FilterByGroup(string group, string functionName, bool expectedExists)
        {
            // setup            
            var uri = QueryHelpers.AddQueryString($"doc/project1/2.0.0/en/functions", "groupFilter", group);

            // action
            var response = await client.GetAsync(uri);

            // asserts
            response.Should().Be200Ok();
            var contentString = await response.Content.ReadAsStringAsync();
            var jobject = JToken.Parse(contentString);
            var items = jobject.Should().HaveElement("items").Which.Should().BeOfType<JArray>().Which;
            if (expectedExists)
                items.Where(i => (string)i["systemName"] == functionName).Should().ContainSingle();
            else
                items.Where(i => (string)i["systemName"] == functionName).Should().BeEmpty();
        }

        [Theory]
        [InlineData("tion2", "test_function2", true)]
        [InlineData("tion2", "test_function1", false)]
        public async Task GetAllFunctions_FilterByName(string filter, string functionName, bool expectedExists)
        {
            // setup            
            var uri = QueryHelpers.AddQueryString($"doc/project1/2.0.0/en/functions", "nameFilter", filter);

            // action
            var response = await client.GetAsync(uri);

            // asserts
            response.Should().Be200Ok();
            var contentString = await response.Content.ReadAsStringAsync();
            var jobject = JToken.Parse(contentString);
            var items = jobject.Should().HaveElement("items").Which.Should().BeOfType<JArray>().Which;
            if (expectedExists)
                items.Where(i => (string)i["systemName"] == functionName).Should().ContainSingle();
            else
                items.Where(i => (string)i["systemName"] == functionName).Should().BeEmpty();
        }

        [Fact]
        public async Task GetAllFunctions_UnknownProject_ShouldBeNotFound()
        {
            // setup

            // action
            var response = await client.GetAsync($"doc/unknown/1.0.0/en/functions");

            // asserts
            response.Should().Be404NotFound().And.BeAs(new { title = "project not found" });
        }
    }
}
