﻿using FluentAssertions;
using FluentAssertions.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace SPR.Documentation.WebService.IntegrationTests.Doc
{
    public class GetGroupsTests : BaseTests
    {
        public GetGroupsTests(WebFactoryBase fixture, ITestOutputHelper testOutput) : base(fixture, testOutput)
        {
        }

        [Theory]
        [InlineData("en", "project1", "1.0.0", "Group 1")]
        [InlineData("ru", "project1", "1.0.0", "Группа 1")]
        public async Task GetGroupsListTest(string lang, string project, string version, string expectedTitle)
        {
            // setup

            // action
            var response = await client.GetAsync($"doc/{project}/{version}/{lang}/groups");

            // asserts
            response.Should().Be200Ok();

            var contentString = await response.Content.ReadAsStringAsync();
            var jobject = JToken.Parse(contentString);
            var items = jobject.Should().HaveElement("items").Which.Should().BeOfType<JArray>().Which;
            var item = items.Where(i => (string)i["group"] == "group1").Should().ContainSingle().Which;
            item.Should().HaveElement("title").Which.Value<string>().Should().Be(expectedTitle);
        }
    }
}
