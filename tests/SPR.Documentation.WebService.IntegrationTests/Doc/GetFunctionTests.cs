﻿using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace SPR.Documentation.WebService.IntegrationTests.Doc
{
    public class GetFunctionTests : BaseTests
    {
        public GetFunctionTests(WebFactoryBase fixture, ITestOutputHelper testOutput) : base(fixture, testOutput)
        {
        }

        [Theory]
        [InlineData("en", "project1", "1.0.0", "en")]
        [InlineData("ru", "project1", "1.0.0", "ru")]
        [InlineData("hz", "project1", "1.0.0", "en")]
        [InlineData("en", "project1", "3.0.0", "en")]
        [InlineData("en", "project2", "3.0.0", "en")]
        public async Task GetFunctionTest(string language, string project, string version, string expectedLanguage)
        {
            // setup
            var dbitem = await context.FunctionNames.FirstOrDefaultAsync(i => i.SystemName == "test_function1");

            // action
            var response = await client.GetAsync($"doc/{project}/{version}/{language}/functions/test_function1");

            // asserts
            response.Should().Be200Ok().And.BeAs(new {
                systemName = dbitem.SystemName,
                title = dbitem.TitleResource.Rows.First(i => i.Language.Code == expectedLanguage).Text,
                //groups = dbitem.Groups.Select(i => i.Alias).ToArray(),
                implementations = dbitem.Functions.Select(i => new
                {
                    description = i.DescriptionResource.Rows.First(i => i.Language.Code == expectedLanguage).Text,
                    args = Array.Empty<object>(),
                    @return = new
                    {
                        i.Return.Type
                    }
                }).ToArray()                
            });
        }

        [Theory]
        [InlineData("project1", "3.0.0")]
        public async Task GetFunctionWithArgsTest(string project, string version)
        {
            // setup
            var dbitem = await context.FunctionNames.FirstOrDefaultAsync(i => i.SystemName == "test_function2");

            // action
            var response = await client.GetAsync($"doc/{project}/{version}/en/functions/test_function2");

            // asserts
            response.Should().Be200Ok().And.BeAs(new
            {
                systemName = dbitem.SystemName,
                implementations = dbitem.Functions.OrderBy(i => i.ArgsCount).Select(i => new
                {
                    description = i.DescriptionResource.Rows.First(i => i.Language.Code == "en").Text,
                    args = i.Arguments.Select(a => new { 
                        type = a.Type,
                        name = a.Name
                    }).ToArray()
                }).ToArray()
            });
        }

        [Theory]
        [InlineData("project1", "3.0.0", 2)]
        [InlineData("project1", "1.0.0", 1)]
        [InlineData("project2", "3.0.0", 1)]
        public async Task FunctionImplementationVersionDependencyTest(string project, string version, int expectedCount)
        {
            // setup
            var dbitem = await context.FunctionNames.FirstOrDefaultAsync(i => i.SystemName == "test_function2");

            // action
            var response = await client.GetAsync($"doc/{project}/{version}/en/functions/test_function2");

            // asserts
            response.Should().Be200Ok();
            var json = JObject.Parse(await response.Content.ReadAsStringAsync());
            json.Should()
                .ContainKey("implementations").WhoseValue.Should()
                .BeOfType<JArray>().Which.Count().Should()
                .Be(expectedCount);
        }

        [Theory]
        [InlineData("project1", "2.0.0", true)]
        [InlineData("project1", "1.0.0", false)]
        [InlineData("project2", "3.0.0", false)]
        public async Task RequestFunctionWithoutImplementations_ShouldBeNoContent(string project, string version, bool expectedExistance)
        {
            // setup
            var dbitem = await context.FunctionNames.FirstOrDefaultAsync(i => i.SystemName == "test_function3");

            // action
            var response = await client.GetAsync($"doc/{project}/{version}/en/functions/test_function3");

            // asserts
            if (expectedExistance)
                response.Should().Be200Ok();
            else
                response.Should().Be204NoContent();
        }

        [Theory]
        [InlineData("unknown")]
        public async Task RequestUnknownProject_ShouldBeNotFound(string project)
        {
            // setup

            // action
            var response = await client.GetAsync($"doc/{project}/ 1.0.0/en/functions/test_function1");

            // asserts
            response.Should().Be404NotFound().And.BeAs(new { title = "project not found" });
        }
    }
}
