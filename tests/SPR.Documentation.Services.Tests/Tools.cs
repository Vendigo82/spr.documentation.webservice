﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Newtonsoft.Json;
using SPR.Documentation.DAL;
using SPR.Documentation.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.Services
{
    public static class Tools
    {
        public static DocContext InitMemoryDd()
        {
            var name = Guid.NewGuid().ToString();

            var optBuilder = new DbContextOptionsBuilder<UnitTestDocContext>()
                //.UseNpgsql()
                .UseInMemoryDatabase(name)
                .ConfigureWarnings(w => w.Ignore(InMemoryEventId.TransactionIgnoredWarning));
            return new UnitTestDocContext(optBuilder.Options);
        }

    }

    public class UnitTestDocContext : DocContext
    {
        public UnitTestDocContext(DbContextOptions<UnitTestDocContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Function>().Property(p => p.Arguments)
                .HasConversion(v => JsonConvert.SerializeObject(v), v => JsonConvert.DeserializeObject<FunctionArgument[]>(v));

            modelBuilder.Entity<Function>().Property(p => p.Return)
                .HasConversion(v => JsonConvert.SerializeObject(v), v => JsonConvert.DeserializeObject<FunctionReturnValue>(v));

            modelBuilder.Entity<ProjectVersion>().Property(p => p.Version)
                .HasConversion(v => JsonConvert.SerializeObject(v), v => JsonConvert.DeserializeObject<int[]>(v));

            modelBuilder.Entity<ProjectVersion>().Property(p => p.LibraryVersion)
                .HasConversion(v => JsonConvert.SerializeObject(v), v => JsonConvert.DeserializeObject<int[]>(v));

            modelBuilder.Entity<DAL.Model.Version>().Property(p => p.VersionValue)
                .HasConversion(v => JsonConvert.SerializeObject(v), v => JsonConvert.DeserializeObject<int[]>(v));
        }
    }
}
