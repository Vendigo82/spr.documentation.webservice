﻿using FluentAssertions;
using SPR.Documentation.DataContract.Editor;
using SPR.Documentation.Services.Tests.ModelCustomizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.Services.GroupsEditorServiceTests
{
    public class UpdateAsyncTests : GroupsEditorServiceBaseTests
    {
        [Theory, AutoModelData]
        public async Task UpdateShouldBeSuccess(DAL.Model.FunctionGroup dbitem, GroupModel model, List<string> titles)
        {
            // setup
            model.Id = dbitem.Id;
            int index = 0;
            model.Titles = context.Languages.ToList().Select(i => new TextResource { Lang = i.Code, Text = titles[index++] }).ToArray();

            await context.FunctionGroups.AddAsync(dbitem);
            await context.SaveChangesAsync();

            // action
            await target.UpdateAsync(model);

            // asserts
            dbitem = context.FunctionGroups.Should().ContainSingle(i => i.Id == dbitem.Id).Which;

            dbitem.Should().BeEquivalentTo(model, o => o.Excluding(p => p.Titles));
            dbitem.TitleResource.Rows.Select(i => new { Lang = i.Language.Code, i.Text }).Should().BeEquivalentTo(model.Titles);
        }

        [Theory, AutoModelData]
        public async Task UpdateNotExistedGroup_ShouldBeException(GroupModel model)
        {
            // setup

            // action
            Func<Task> action = () => target.UpdateAsync(model);

            // assets
            await action.Should().ThrowExactlyAsync<Exceptions.GroupNotFoundException>();
        }

        [Theory, AutoModelData]
        public async Task UpdateWithUnknownLanguage_ShouldBeException(DAL.Model.FunctionGroup dbitem, GroupModel model, string langCode)
        {
            // setup
            model.Id = dbitem.Id;
            await context.FunctionGroups.AddAsync(dbitem);
            await context.SaveChangesAsync();

            model.Titles = new List<TextResource>() { new TextResource { Lang = langCode, Text = "title" } };

            // action
            Func<Task> action = () => target.UpdateAsync(model);

            // assets
            var e = (await action.Should().ThrowExactlyAsync<Exceptions.LanguageNotFoundException>()).Which;
            e.LanguageCode.Should().Be(langCode);
        }
    }
}
