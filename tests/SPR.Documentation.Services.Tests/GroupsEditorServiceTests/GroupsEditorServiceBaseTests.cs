﻿using AutoMapper;
using SPR.Documentation.Services.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.Services.GroupsEditorServiceTests
{
    public class GroupsEditorServiceBaseTests : ContextBase
    {
        protected readonly GroupsEditorService target;

        public GroupsEditorServiceBaseTests()
        {
            var mapper = new Mapper(new MapperConfiguration(p => p.AddProfile<MappingProfile>()));
            target = new GroupsEditorService(context, mapper);
        }
    }
}
