﻿using FluentAssertions;
using SPR.Documentation.DataContract.Editor;
using SPR.Documentation.Services.Tests.ModelCustomizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.Services.GroupsEditorServiceTests
{
    public class InsertAsyncTests : GroupsEditorServiceBaseTests
    {
        [Theory, AutoModelData]
        public async Task InsertNewGroup_ShouldBeSuccess(GroupModel model)
        {
            // setup

            // action
            var result = await target.InsertAsync(model);

            // asserts
            result.Should().Be(model.Id);

            var dbitem = context.FunctionGroups.Should().ContainSingle(i => i.Id == result).Which;
            dbitem.Should().BeEquivalentTo(model, o => o.Excluding(p => p.Titles));
        }

        [Theory, AutoModelData]
        public async Task InsertNewGroupWithTitles_ShouldBeSuccess(GroupModel model, List<string> titles)
        {
            // setup
            int index = 0;
            model.Titles = context.Languages.ToList().Select(i => new TextResource { Lang = i.Code, Text = titles[index++] }).ToArray();

            // action
            var result = await target.InsertAsync(model);

            // asserts
            result.Should().Be(model.Id);

            var dbitem = context.FunctionGroups.Should().ContainSingle(i => i.Id == result).Which;
            dbitem.TitleResource.Rows.Select(i => new { Lang = i.Language.Code, i.Text }).Should().BeEquivalentTo(model.Titles);
        }

        [Theory, AutoModelData]
        public async Task TryToInsertNewGroupWithUnknownLanguage_ShouldBeException(GroupModel model)
        {
            // setup
            model.Titles = new List<TextResource>() { new TextResource { Lang = "xyz", Text = "title" } };

            // action
            Func<Task> action = () => target.InsertAsync(model);

            // asserts
            (await action.Should().ThrowExactlyAsync<Exceptions.LanguageNotFoundException>()).Which.LanguageCode.Should().Be("xyz");
        }
    }
}
