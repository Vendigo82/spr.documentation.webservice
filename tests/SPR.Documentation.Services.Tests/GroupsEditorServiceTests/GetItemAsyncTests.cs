﻿using AutoFixture.Xunit2;
using FluentAssertions;
using SPR.Documentation.DAL.Model;
using SPR.Documentation.Services.Tests.ModelCustomizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.Services.GroupsEditorServiceTests
{
    public class GetItemAsyncTests : GroupsEditorServiceBaseTests
    {
        [Theory, AutoModelData]
        public async Task GetItemAsync_ShouldBeFound(FunctionGroup dbitem, IEnumerable<TextResourceRow> rows)
        {
            // setup
            dbitem.TitleResource.Rows = rows.ToList();
            await context.FunctionGroups.AddAsync(dbitem);
            await context.SaveChangesAsync();

            // action
            var result = await target.GetItemAsync(dbitem.Id);

            // asserts           
            dbitem.Should().BeEquivalentTo(result, o => o.Excluding(p => p.Titles));
            result.Titles.Should().BeEquivalentTo(rows.Select(i => new { Lang = i.Language.Code, i.Text }));
        }

        [Theory, AutoData]
        public async Task GetItemAsync_ItemNotFound_ShouldReturnsNull(Guid groupId)
        {
            // setup

            // action
            var result = await target.GetItemAsync(groupId);

            // asserts
            result.Should().BeNull();
        }
    }
}
