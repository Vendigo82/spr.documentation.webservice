﻿using FluentAssertions;
using SPR.Documentation.DAL.Model;
using SPR.Documentation.Services.Tests.ModelCustomizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.Services.GroupsEditorServiceTests
{
    public class GetGroupsListTests : GroupsEditorServiceBaseTests
    {
        [Theory, AutoModelData]
        public async Task GetList_ShouldBeSuccess(IEnumerable<FunctionGroup> dbitems)
        {
            // setup
            await context.FunctionGroups.AddRangeAsync(dbitems);
            await context.SaveChangesAsync();

            // action
            var result = await target.GetListAsync();

            // asserts
            result.Should()
                .NotBeNull().And
                .HaveSameCount(dbitems).And
                .BeInAscendingOrder(p => p.Alias).And.Subject                
                .Select(i => i.Id).Should().BeEquivalentTo(dbitems.Select(i => i.Id));
        }

        [Theory, AutoModelData]
        public async Task GetList_ValidateMapping(FunctionGroup dbitem)
        {
            // setup
            await context.FunctionGroups.AddAsync(dbitem);
            await context.SaveChangesAsync();

            // action
            var result = await target.GetListAsync();

            // asserts
            var item = result.Should().ContainSingle().Which;
            dbitem.Should().BeEquivalentTo(item);
        }
    }
}
