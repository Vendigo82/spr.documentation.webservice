﻿using AutoFixture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.Services.Tests.ModelCustomizations
{
    public class ModelCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customize<DAL.Model.Language>(c => c.Without(p => p.TextRows));
            fixture.Customize<DAL.Model.TextResourceRow>(c => c.Without(p => p.TextResource));
            fixture.Customize<DAL.Model.TextResource>(c => c.With(p => p.Rows, () => new List<DAL.Model.TextResourceRow>()));
            fixture.Customize<DAL.Model.FunctionExample>(c => c.Without(p => p.Function));
            fixture.Customize<DAL.Model.FunctionVersionAssignment>(c => c.Without(p => p.Function));
            fixture.Customize<DAL.Model.Version>(c => c.Without(p => p.FormulaVersionAssignments));
            fixture.Customize<DAL.Model.FunctionGroup>(c => c.Without(p => p.Functions));
            fixture.Customize<DAL.Model.FunctionName>(c => c.With(p => p.Groups, () => new List<DAL.Model.FunctionGroup>()).Without(p => p.Functions));            

            fixture.Customize<DataContract.Editor.FunctionModel>(c => c
                .With(p => p.Titles, Enumerable.Empty<DataContract.Editor.TextResource>())
                .With(p => p.Descriptions, Enumerable.Empty<DataContract.Editor.TextResource>())
                .Without(p => p.LibraryVersion)
                .Without(p => p.Groups)
                );
            fixture.Customize<DataContract.Editor.ArgumentModel>(c => c.With(p => p.Type, () => fixture.Create<DAL.Model.DataTypeEnum>().ToString()));
            fixture.Customize<DataContract.Editor.ReturnValueModel>(c => c.With(p => p.Type, () => fixture.Create<DAL.Model.DataTypeEnum>().ToString()));

            fixture.Customize<DataContract.Editor.GroupModel>(c => c.With(p => p.Titles, Enumerable.Empty<DataContract.Editor.TextResource>()));
        }
    }

    public class AutoModelDataAttribute : AutoFixture.Xunit2.AutoDataAttribute
    {
        public AutoModelDataAttribute() : base(Create)
        {
        }

        protected static IFixture Create()
        {
            var fixture = new Fixture();

            //fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
            //    .ForEach(b => fixture.Behaviors.Remove(b));
            //fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            fixture.Customize(new ModelCustomization());
            return fixture;
        }
    }

    public class InlineAutoModelDataAttribute : AutoFixture.Xunit2.InlineAutoDataAttribute
    {
        public InlineAutoModelDataAttribute(params object[] values) : base(new AutoModelDataAttribute(), values)
        {
        }
    }
}
