﻿using FluentAssertions.Equivalency;

namespace FluentAssertions
{
    public class EnumWithStringEquivalency<TEnum> : IEquivalencyStep
    {
        public EquivalencyResult Handle(Comparands comparands, IEquivalencyValidationContext context, IEquivalencyValidator nestedValidator)
        {
            if (comparands.Subject.GetType() == typeof(TEnum) && comparands.Expectation.GetType() == typeof(string))
            {
                comparands.Subject.ToString().Should().Be((string)comparands.Expectation);
                return EquivalencyResult.AssertionCompleted;
            }

            if (comparands.Subject.GetType() == typeof(string) && comparands.Expectation.GetType() == typeof(TEnum))
            {
                comparands.Subject.Should().Be(comparands.Expectation.ToString());
                return EquivalencyResult.AssertionCompleted;
            }

            return EquivalencyResult.ContinueWithNext;
        }
    }
}
