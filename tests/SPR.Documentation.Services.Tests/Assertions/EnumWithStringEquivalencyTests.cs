﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Sdk;

namespace SPR.Documentation.Services.Tests.Assertions
{
    public class EnumWithStringEquivalencyTests
    {
        public enum TestEnum { Value1, Value2 }

        public class EnumClass
        {
            public int AValue { get; set; }

            public TestEnum EnumValue { get; set; }

            public int ZValue { get; set; }
        }

        public class StringValue
        {
            public int AValue { get; set; }

            public string EnumValue { get; set; }

            public int ZValue { get; set; }
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void CompareWithoutEquivalencyShouldBeNotEquivalent(bool direction)
        {
            // setup
            var e = new EnumClass { EnumValue = TestEnum.Value1 };
            var s = new StringValue { EnumValue = TestEnum.Value1.ToString() };

            // action
            Action action;
            if (direction)
                action = () => e.Should().BeEquivalentTo(s);
            else
                action = () => s.Should().BeEquivalentTo(e);

            // asserts
            action.Should().Throw<XunitException>();
        }

        [Theory]
        [InlineData(TestEnum.Value1, "Value1", true)]
        [InlineData(TestEnum.Value2, "Value2", true)]
        [InlineData(TestEnum.Value2, "Value1", false)]
        [InlineData(TestEnum.Value1, "xx", false)]
        public void EnumWithStringEquivalencyTest(TestEnum enumValue, string stringValue, bool expectedEqual)
        {
            // setup
            var e = new EnumClass { EnumValue = enumValue };
            var s = new StringValue { EnumValue = stringValue };

            // action
            Action action = () => e.Should().BeEquivalentTo(s, o => o.Using(new EnumWithStringEquivalency<TestEnum>()));

            // asserts
            if (expectedEqual)
                action.Should().NotThrow();
            else
                action.Should().Throw<XunitException>();
        }

        [Theory]
        [InlineData(TestEnum.Value1, "Value1", true)]
        [InlineData(TestEnum.Value2, "Value2", true)]
        [InlineData(TestEnum.Value2, "Value1", false)]
        [InlineData(TestEnum.Value1, "xx", false)]
        public void StringWithEnumEquivalencyTest(TestEnum enumValue, string stringValue, bool expectedEqual)
        {
            // setup
            var e = new EnumClass { EnumValue = enumValue };
            var s = new StringValue { EnumValue = stringValue };
            
            // action
            Action action = () => s.Should().BeEquivalentTo(e, o => o.Using(new EnumWithStringEquivalency<TestEnum>()));

            // asserts
            if (expectedEqual)
                action.Should().NotThrow();
            else
                action.Should().Throw<XunitException>();
        }
    }
}
