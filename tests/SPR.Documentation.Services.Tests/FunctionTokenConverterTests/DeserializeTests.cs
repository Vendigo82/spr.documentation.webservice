﻿using AutoFixture.Xunit2;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.Services.Tests.FunctionTokenConverterTests
{
    public class DeserializeTests
    {
        readonly FunctionTokenConverter target = new();

        [Theory, AutoData]
        public void SuccessDeserializeTest(string functionName, int args)
        {
            // setup
            var bytes = Encoding.UTF8.GetBytes($"ftoken1:{functionName}:{args}");
            var token = Convert.ToBase64String(bytes);

            // action
            var result = target.Deserialize(token);

            // asserts
            result.Should().BeEquivalentTo(new {
                SystemName = functionName,
                ArgsCount = args
            });
        }

        [Theory, MemberData(nameof(InvalidToken_ShouldBeExceptionTestCases))]
        public void InvalidToken_ShouldBeExceptionTest(string token)
        {
            // setup

            // action
            Action action = () => target.Deserialize(token);

            // asserts
            action.Should().ThrowExactly<Exceptions.InvalidTokenException>();
        }

        public static IEnumerable<object[]> InvalidToken_ShouldBeExceptionTestCases()
        {
            yield return new object[] { "@@@###" }; //invalid encoding
            yield return new object[] { MakeToken("ftoken1:function") };   // invalid segments count
            yield return new object[] { MakeToken("ftoken:function:1") };   // invalid token version
            yield return new object[] { MakeToken("ftoken1:function:aa") };   // invalid format of args cound

            static string MakeToken(string source)
            {
                var bytes = Encoding.UTF8.GetBytes(source);
                return Convert.ToBase64String(bytes);
            }
        }
    }
}
