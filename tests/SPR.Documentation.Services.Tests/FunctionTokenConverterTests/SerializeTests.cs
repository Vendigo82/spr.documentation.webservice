﻿using AutoFixture.Xunit2;
using FluentAssertions;
using SPR.Documentation.DataContract.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.Services.Tests.FunctionTokenConverterTests
{
    public class SerializeTests
    {
        readonly FunctionTokenConverter target = new();

        [Theory, AutoData]
        public void SerializeDeserializeTest(FunctionBriefModel model)
        {
            // setup

            // action
            var token = target.Serialize(model);
            var result = target.Deserialize(token);

            // asserts
            result.Should().BeEquivalentTo(model, o => o.Excluding(p => p.Id));
        }
    }
}
