﻿using AutoMapper;
using SPR.Documentation.Services.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.Services.Tests.Mapping
{
    public class MappingProfileTests
    {
        [Fact]
        public void ValidateConfigurationTest()
        {
            var cfg = new MapperConfiguration(c => c.AddProfile<MappingProfile>());
            cfg.AssertConfigurationIsValid();
        }
    }
}
