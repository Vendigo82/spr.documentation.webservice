﻿using AutoFixture.Xunit2;
using FluentAssertions;
using SPR.Documentation.DAL.Model;
using SPR.Documentation.Services.Tests.ModelCustomizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.Services.Tests.FunctionsEditorServiceTests
{
    public class GetFunctionAsyncTests : FunctionsEditorServiceBaseTests
    {
        [Theory, AutoModelData]
        public async Task SuccessTest(Function model)
        {
            // setup
            await context.Functions.AddAsync(model);
            await context.SaveChangesAsync();

            // action
            var result = await target.GetFunctionAsync(model.Id);

            // asserts
            result.Should().NotBeNull();
            model.Should().BeEquivalentTo(result, o => o
                .ComparingEnumsByName()
                .WithAutoConversionFor(x => x.Path.EndsWith("Type"))
                .Excluding(p => p.Titles)
                .Excluding(p => p.Descriptions)
                .Excluding(p => p.SystemName)
                .Excluding(p => p.LibraryVersion)     
                .Excluding(p => p.Groups)
                );

            result.SystemName.Should().Be(model.FunctionName.SystemName);
            result.Titles.Should().BeEquivalentTo(model.FunctionName.TitleResource.Rows.Select(i => new { Lang = i.Language.Code, i.Text }));
            result.Descriptions.Should().BeEquivalentTo(model.DescriptionResource.Rows.Select(i => new { Lang = i.Language.Code, i.Text }));
        }

        [Theory, AutoModelData]
        public async Task GetFunction_GroupsFieldsShouldBeFilled(Function model, IEnumerable<FunctionGroup> groups)
        {
            // setup
            model.FunctionName.Groups.AddRange(groups);
            await context.Functions.AddAsync(model);
            await context.SaveChangesAsync();

            // action
            var result = await target.GetFunctionAsync(model.Id);

            // asserts
            var expectedGroupsIds = model.FunctionName.Groups.Select(i => i.Id);
            result.Groups.Should().NotBeNullOrEmpty().And.BeEquivalentTo(expectedGroupsIds);
        }

        [Theory]
        [InlineAutoModelData(true)]
        [InlineAutoModelData(false)]
        public async Task CompareLibraryVersionTest(bool withLibraryVersion, Function model)
        {
            // setup
            if (!withLibraryVersion)
                model.VersionAssignment = null;

            await context.Functions.AddAsync(model);
            await context.SaveChangesAsync();

            // action
            var result = await target.GetFunctionAsync(model.Id);

            // asserts
            if (withLibraryVersion)
                result.LibraryVersion.Should().Be(string.Join('.', model.VersionAssignment.Version.VersionValue));
            else
                result.LibraryVersion.Should().BeNull();
        }

        [Theory, AutoData]
        public async Task NotFoundTest(Guid functionId)
        {
            // setup

            // action
            var result = await target.GetFunctionAsync(functionId);

            // asserts
            result.Should().BeNull();
        }
    }
}
