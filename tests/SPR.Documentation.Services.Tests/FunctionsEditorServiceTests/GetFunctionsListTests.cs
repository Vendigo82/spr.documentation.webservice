﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.Services.Tests.FunctionsEditorServiceTests
{
    public class GetFunctionsListTests : FunctionsEditorServiceBaseTests
    {
        public GetFunctionsListTests()
        {
            var items = Enumerable.Range(0, 3).Select(i => new DAL.Model.Function {
                Id = Guid.NewGuid(),
                ArgsCount = (short)i,
                Arguments = new DAL.Model.FunctionArgument[] { },
                FunctionName = new DAL.Model.FunctionName { Id = Guid.NewGuid(), SystemName = "test" + i.ToString() },
            });
            context.Functions.AddRange(items);
            context.SaveChanges();
        }

        [Fact]
        public async Task GetListTest()
        {
            // setup

            // action
            var result = await target.GetFunctionsListAsync(new Model.FunctionsRequestOptions { });

            // asserts
            result.Should()
                .HaveSameCount(context.Functions).And
                .BeInAscendingOrder(i => i.SystemName).And
                .Subject.Select(i => i.Id).Should().BeEquivalentTo(context.Functions.Select(i => i.Id));
        }

        [Fact]
        public async Task MappingTest()
        {
            // setup

            // action
            var result = await target.GetFunctionsListAsync(new Model.FunctionsRequestOptions { });

            // asserts
            result.Should().NotBeEmpty();
            var item = result.First();

            var dbitem = context.Functions.First(i => i.Id == item.Id);
            dbitem.Should().BeEquivalentTo(dbitem);
        }

        [Theory]
        [InlineData("aaa", 3)]
        [InlineData("test1", 2)]
        [InlineData("test2", 1)]
        [InlineData("test3", 0)]
        public async Task StartsFromTest(string startFrom, int expectedCount)
        {
            // setup
            var options = new Model.FunctionsRequestOptions {
                StartsFrom = new DataContract.Editor.FunctionBriefModel {
                    SystemName = startFrom
                }
            };

            // action
            var result = await target.GetFunctionsListAsync(options);

            // asserts
            result.Should().HaveCount(expectedCount);
        }

        [Theory]
        [InlineData(0, "test0")]
        [InlineData(1, "test1")]
        [InlineData(2, "test2")]
        public async Task OffsetTest(int offset, string expectedFirstName)
        {
            // setup
            var options = new Model.FunctionsRequestOptions {
                Offset = offset,                
            };

            // action
            var result = await target.GetFunctionsListAsync(options);

            // asserts
            result.Select(i => i.SystemName).Should().NotBeEmpty().And.StartWith(expectedFirstName);
        }

        [Theory]
        [InlineData("abc", 0)]
        [InlineData("test", 3)]
        [InlineData("1", 1)]
        [InlineData("st1", 1)]
        public async Task FilterTest(string filter, int expectedCount)
        {
            // setup
            var options = new Model.FunctionsRequestOptions {
                Filter = filter
            };

            // action
            var result = await target.GetFunctionsListAsync(options);

            // asserts
            result.Should().HaveCount(expectedCount);
            if (expectedCount > 0)
                result.Should().OnlyContain(i => i.SystemName.Contains(filter));
        }

        [Theory]
        [InlineData(5, 3)]
        [InlineData(3, 3)]
        [InlineData(2, 2)]
        public async Task MaxCountTest(int maxCount, int expectedCount)
        {
            // setup
            var options = new Model.FunctionsRequestOptions {
                MaxCount = maxCount
            };

            // action
            var result = await target.GetFunctionsListAsync(options);

            // asserts
            result.Should().HaveCount(expectedCount);
        }
    }
}
