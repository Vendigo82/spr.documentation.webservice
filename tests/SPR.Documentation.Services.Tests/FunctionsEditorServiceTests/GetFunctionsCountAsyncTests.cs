﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.Services.Tests.FunctionsEditorServiceTests
{
    public class GetFunctionsCountAsyncTests : FunctionsEditorServiceBaseTests
    {
        public GetFunctionsCountAsyncTests()
        {
            var items = Enumerable.Range(0, 3).Select(i => new DAL.Model.Function {
                Id = Guid.NewGuid(),
                ArgsCount = (short)i,
                Arguments = new DAL.Model.FunctionArgument[] { },
                FunctionName = new DAL.Model.FunctionName { Id = Guid.NewGuid(), SystemName = "test" + i.ToString() },
            });
            context.Functions.AddRange(items);
            context.SaveChanges();
        }

        [Fact]
        public async Task WithoutFilterTest()
        {
            // setup

            // action
            var count = await target.GetFunctionsCountAsync(null);

            // asserts
            count.Should().Be(context.Functions.Count());
        }

        [Theory]
        [InlineData("test", 3)]
        [InlineData("test1", 1)]
        public async Task WithFilterTest(string filter, int expectedCount)
        {
            // setup

            // action
            var count = await target.GetFunctionsCountAsync(filter);

            // asserts
            count.Should().Be(expectedCount);
        }
    }
}
