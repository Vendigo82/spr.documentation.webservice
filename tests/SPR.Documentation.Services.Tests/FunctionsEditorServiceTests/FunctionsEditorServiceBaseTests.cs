﻿using AutoMapper;
using SPR.Documentation.DAL;
using SPR.Documentation.Services.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.Services.Tests.FunctionsEditorServiceTests
{
    public class FunctionsEditorServiceBaseTests
    {
        protected readonly DocContext context;
        protected readonly FunctionsEditorService target;

        public FunctionsEditorServiceBaseTests()
        {
            context = Tools.InitMemoryDd();
            var mapper = new Mapper(new MapperConfiguration(p => p.AddProfile<MappingProfile>()));
            target = new FunctionsEditorService(context, mapper);
        }
    }
}
