﻿using FluentAssertions;
using FluentAssertions.Equivalency;
using FluentAssertions.Reflection;
using SPR.Documentation.DataContract.Editor;
using SPR.Documentation.Services.Tests.ModelCustomizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.Services.Tests.FunctionsEditorServiceTests
{
    public class InsertFunctionAsyncTests : FunctionsEditorServiceBaseTests
    {
        public InsertFunctionAsyncTests()
        {
            context.Languages.Add(new DAL.Model.Language { Code = "en", Title = "English" });
            context.Languages.Add(new DAL.Model.Language { Code = "ru", Title = "Russian" });
            context.Languages.Add(new DAL.Model.Language { Code = "cn", Title = "Chinese" });
            context.SaveChanges();
        }

        [Theory, AutoModelData]
        public async Task ShouldBeSuccessInsert(FunctionModel model)
        {
            // setup

            // action
            var result = await target.InsertFunctionAsync(model);

            // asserts
            result.Should().Be(model.Id);
            var dbitem = context.Functions.Should().ContainSingle(i => i.Id == result).Which;

            dbitem.Should().BeEquivalentTo(model, o => o
                .Using(new EnumWithStringEquivalency<DAL.Model.DataTypeEnum>())
                .Excluding(p => p.SystemName)
                .Excluding(p => p.Titles)   // see separate test below
                .Excluding(p => p.Descriptions) // see separate test below
                .Excluding(p => p.LibraryVersion) // see separate test below
                .Excluding(p => p.Groups)
                ); 

            dbitem.FunctionName.SystemName.Should().Be(model.SystemName);
            dbitem.FunctionName.Groups.Should().BeEmpty();
            dbitem.VersionAssignment.Should().BeNull();
        }

        [Theory, AutoModelData]
        public async Task EmptyIdShoudBeFilled(FunctionModel model)
        {
            // setup
            model.Id = Guid.Empty;

            // action
            var result = await target.InsertFunctionAsync(model);

            // asserts
            result.Should().NotBeEmpty();
            context.Functions.Should().ContainSingle(i => i.Id == result);
        }

        [Theory, AutoModelData]
        public async Task TitlesForNewFunctionNameSouldBeFilled(FunctionModel model, List<string> titles)
        {
            // action
            int index = 0;
            model.Titles = context.Languages.ToList().Select(i => new TextResource { Lang = i.Code, Text = titles[index++] }).ToArray();

            // action
            var result = await target.InsertFunctionAsync(model);

            // asserts
            var dbitem = context.Functions.Should().ContainSingle(i => i.Id == result).Which;

            dbitem.FunctionName.TitleResource.Should().NotBeNull();
            var expected = model.Titles.Select(i => new { Language = new { Code = i.Lang }, i.Text });
            dbitem.FunctionName.TitleResource.Rows.Should().NotBeEmpty().And.BeEquivalentTo(expected);
        }

        [Theory, AutoModelData]
        public async Task DescriptionsSouldBeFilled(FunctionModel model, List<string> titles)
        {
            // action
            int index = 0;
            model.Descriptions = context.Languages.ToList().Select(i => new TextResource { Lang = i.Code, Text = titles[index++] }).ToArray();

            // action
            var result = await target.InsertFunctionAsync(model);

            // asserts
            var dbitem = context.Functions.Should().ContainSingle(i => i.Id == result).Which;

            dbitem.DescriptionResource.Should().NotBeNull();
            var expected = model.Descriptions.Select(i => new { Language = new { Code = i.Lang }, i.Text });
            dbitem.DescriptionResource.Rows.Should().NotBeEmpty().And.BeEquivalentTo(expected);
        }

        [Theory, AutoModelData]
        public async Task LibraryVersionShoudBeAssigned(FunctionModel model, int[] libraryVersion)
        {
            // setup
            var dbversion = await context.Versions.AddAsync(new DAL.Model.Version { VersionValue = libraryVersion });
            await context.SaveChangesAsync();

            model.LibraryVersion = string.Join('.', libraryVersion.Select(i => i.ToString()));

            // action
            var result = await target.InsertFunctionAsync(model);

            // asserts
            var dbitem = context.Functions.Should().ContainSingle(i => i.Id == result).Which;
            dbitem.VersionAssignment.Should().NotBeNull();
            dbitem.VersionAssignment.Version.Id.Should().Be(dbversion.Entity.Id);
        }

        [Theory, AutoModelData]
        public async Task ShouldUseAlreadyExistedFunctionName(FunctionModel model)
        {
            // setup
            var dbFunctionName = await context.FunctionNames.AddAsync(new DAL.Model.FunctionName {
                SystemName = model.SystemName, 
                TitleResource = new DAL.Model.TextResource { } 
            });
            await context.SaveChangesAsync();

            // action
            var result = await target.InsertFunctionAsync(model);

            // asserts
            var dbitem = context.Functions.Should().ContainSingle(i => i.Id == result).Which;
            dbitem.FunctionName.Id.Should().Be(dbFunctionName.Entity.Id);

            context.FunctionNames.Should().ContainSingle();
        }

        [Theory]
        [InlineAutoModelData("ru", "ru")]
        [InlineAutoModelData("ru", "en")]
        [InlineAutoModelData("ru", "ru", "sametitle", "sametitle")]
        [InlineAutoModelData("ru", null)]
        [InlineAutoModelData(null, "ru")]
        public async Task ShouldUpdateTitlesInAlreadyExistedFunctionName(string oldLand, string newLand, string oldTitle, string newTitle, FunctionModel model) 
        {
            // setup
            var dbFunctionName = await context.FunctionNames.AddAsync(new DAL.Model.FunctionName {
                SystemName = model.SystemName,
                TitleResource = new DAL.Model.TextResource {
                    Rows = oldLand != null
                        ? new List<DAL.Model.TextResourceRow> {
                            new DAL.Model.TextResourceRow {
                                LanguageId = context.Languages.First(i => i.Code == oldLand).Id,
                                Text = oldTitle
                            }
                        }
                        : new List<DAL.Model.TextResourceRow>()
                }
            });
            await context.SaveChangesAsync();

            if (newLand != null)
                model.Titles = new List<TextResource> { new TextResource { Lang = newLand, Text = newTitle } };

            // action
            var result = await target.InsertFunctionAsync(model);

            // asserts
            var dbitem = context.Functions.Should().ContainSingle(i => i.Id == result).Which;

            dbitem.FunctionName.TitleResource.Should().NotBeNull();
            if (newLand != null)
            {
                var expected = model.Titles.Select(i => new { Language = new { Code = i.Lang }, i.Text });
                dbitem.FunctionName.TitleResource.Rows.Should().BeEquivalentTo(expected);
            } 
            else
            {
                dbitem.FunctionName.TitleResource.Rows.Should().BeEmpty();
            }
        }

        [Theory, AutoModelData]
        public async Task UnknownLanguageShouldBeException(FunctionModel model)
        {
            // setup
            model.Titles = new[] { new TextResource { Lang = "xyz", Text = "title" } };

            // action
            Func<Task> action = () => target.InsertFunctionAsync(model);

            // asserts
            (await action.Should().ThrowExactlyAsync<Exceptions.LanguageNotFoundException>()).Which.LanguageCode.Should().Be("xyz");
        }

        [Theory, AutoModelData]
        public async Task UnknownVersionShouldBeException(FunctionModel model)
        {
            // setup
            model.LibraryVersion = "4.5.6";

            // action
            Func<Task> action = () => target.InsertFunctionAsync(model);

            // asserts
            (await action.Should().ThrowExactlyAsync<Exceptions.LibraryVersionNotFoundException>()).Which.Version.Should().Be("4.5.6");
        }


        [Theory, AutoModelData]
        public async Task InsertFunctionWithGroups_ShouldBeSuccess(FunctionModel model, IEnumerable<DAL.Model.FunctionGroup> groups)
        {
            // setup
            await context.FunctionGroups.AddRangeAsync(groups);
            await context.SaveChangesAsync();

            model.Groups = groups.Select(i => i.Id).ToArray();

            // action
            var result = await target.InsertFunctionAsync(model);

            // asserts
            var dbitem = context.Functions.Should().ContainSingle(i => i.Id == result).Which;
            dbitem.FunctionName.Groups.Should()
                .NotBeEmpty().And
                .HaveSameCount(groups).And
                .Subject.Select(i => i.Id).Should().BeEquivalentTo(model.Groups);
        }

        [Theory, AutoModelData]
        public async Task InsertFunctionWithGroups_UseAlreadyExistedFunctionName_GroupsShouldBeUpdated(FunctionModel model, List<DAL.Model.FunctionGroup> groups)
        {
            // setup
            await context.FunctionGroups.AddRangeAsync(groups);
            await context.SaveChangesAsync();

            var dbFunctionName = await context.FunctionNames.AddAsync(new DAL.Model.FunctionName {
                SystemName = model.SystemName,
                TitleResource = new DAL.Model.TextResource { },
                Groups = new List<DAL.Model.FunctionGroup> {
                    context.FunctionGroups.First(i => i.Id == groups[0].Id),    // use groups #0 and #1
                    context.FunctionGroups.First(i => i.Id == groups[1].Id)
                }
            });
            await context.SaveChangesAsync();

            model.Groups = groups.Skip(1).Select(i => i.Id).ToArray();  // use groups #1 and #2

            // action
            var result = await target.InsertFunctionAsync(model);

            // asserts
            var dbitem = context.Functions.Should().ContainSingle(i => i.Id == result).Which;
            dbitem.FunctionName.Groups.Should()
                .NotBeEmpty().And
                .HaveCount(groups.Count() - 1).And
                .Subject.Select(i => i.Id).Should().BeEquivalentTo(model.Groups);
        }

        [Theory, AutoModelData]
        public async Task InsertFunctionWithoutGroups_UseAlreadyExistedFunctionName_GroupsShouldNotChanged(FunctionModel model, List<DAL.Model.FunctionGroup> groups)
        {
            // setup
            await context.FunctionGroups.AddRangeAsync(groups);
            await context.SaveChangesAsync();

            var dbFunctionName = await context.FunctionNames.AddAsync(new DAL.Model.FunctionName {
                SystemName = model.SystemName,
                TitleResource = new DAL.Model.TextResource { },
                Groups = groups
            });
            await context.SaveChangesAsync();

            model.Groups = null;  // without groups

            // action
            var result = await target.InsertFunctionAsync(model);

            // asserts
            var dbitem = context.Functions.Should().ContainSingle(i => i.Id == result).Which;
            dbitem.FunctionName.Groups.Should()
                .NotBeEmpty().And
                .HaveSameCount(groups).And
                .Subject.Select(i => i.Id).Should().BeEquivalentTo(groups.Select(i => i.Id));
        }

        [Theory, AutoModelData]
        public async Task InsertFunctionWithUnknownGroups_ShouldBeException(FunctionModel model, IEnumerable<Guid> groups)
        {
            // setup
            model.Groups = groups;

            // action
           Func<Task> action = () => target.InsertFunctionAsync(model);

            // asserts
            (await action.Should().ThrowExactlyAsync<Exceptions.GroupNotFoundException>()).Which.GroupsIds.Should().BeEquivalentTo(groups);
        }
    }
}
