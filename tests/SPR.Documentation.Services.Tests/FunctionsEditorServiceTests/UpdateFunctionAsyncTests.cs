﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using SPR.Documentation.DataContract.Editor;
using SPR.Documentation.Services.Exceptions;
using SPR.Documentation.Services.Tests.ModelCustomizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.Services.Tests.FunctionsEditorServiceTests
{
    public class UpdateFunctionAsyncTests : FunctionsEditorServiceBaseTests
    {
        public UpdateFunctionAsyncTests()
        {
            context.Languages.Add(new DAL.Model.Language { Code = "en", Title = "English" });
            context.Languages.Add(new DAL.Model.Language { Code = "ru", Title = "Russian" });
            context.Languages.Add(new DAL.Model.Language { Code = "cn", Title = "Chinese" });
            context.SaveChanges();
        }

        [Theory, AutoModelData]
        public async Task UpdateFunctionFieldsTest(FunctionModel model, DAL.Model.Function dbitem)
        {
            // setup
            dbitem.Id = model.Id;
            dbitem.FunctionName.SystemName = model.SystemName;
            await context.Functions.AddAsync(dbitem);
            await context.SaveChangesAsync();

            // action
            await target.UpdateFunctionAsync(model);

            // asserts
            dbitem = context.Functions.Should().ContainSingle(i => i.Id == model.Id).Which;

            dbitem.Should().BeEquivalentTo(model, o => o
                .Using(new EnumWithStringEquivalency<DAL.Model.DataTypeEnum>())
                .Excluding(p => p.SystemName)
                .Excluding(p => p.Titles)   // see separate test below
                .Excluding(p => p.Descriptions) // see separate test below
                .Excluding(p => p.LibraryVersion) // see separate test below
                .Excluding(p => p.Groups)
                );

            dbitem.FunctionName.SystemName.Should().Be(model.SystemName);
            dbitem.VersionAssignment.Should().BeNull();
        }

        [Theory]
        [InlineAutoModelData(true)]
        [InlineAutoModelData(false)]
        public async Task UpdateFunctionNameTest(bool hasAlreadyCreatedFunctionName, FunctionModel model, DAL.Model.Function dbitem, DAL.Model.FunctionName existedFunctionName)
        {
            // setup
            if (hasAlreadyCreatedFunctionName)
                context.FunctionNames.Add(existedFunctionName);

            dbitem.Id = model.Id;

            model.SystemName = existedFunctionName.SystemName;

            await context.Functions.AddAsync(dbitem);
            await context.SaveChangesAsync();

            var prevFunctionNameId = dbitem.FunctionNameId;

            // action
            await target.UpdateFunctionAsync(model);

            // asserts
            dbitem = context.Functions.Should().ContainSingle(i => i.Id == model.Id).Which;
            dbitem.FunctionName.SystemName.Should().Be(model.SystemName);
            dbitem.FunctionNameId.Should().NotBe(prevFunctionNameId);
        }

        [Theory, AutoModelData]
        public async Task UpdateFunctionNameTitlesTest(FunctionModel model, DAL.Model.Function dbitem, List<string> newTitles)
        {
            // setup            
            dbitem.Id = model.Id;
            dbitem.FunctionName.SystemName = model.SystemName;
            dbitem.FunctionName.TitleResource.Rows.Add(new DAL.Model.TextResourceRow { LanguageId = context.Languages.First().Id, Text = "txt" });
            await context.Functions.AddAsync(dbitem);
            await context.SaveChangesAsync();

            model.Titles = newTitles.ToModelTitles(context);

            // action
            await target.UpdateFunctionAsync(model);

            // asserts
            dbitem = context.Functions.Should().ContainSingle(i => i.Id == model.Id).Which;

            dbitem.FunctionName.TitleResource.Should().NotBeNull();
            var expected = model.Titles.Select(i => new { Language = new { Code = i.Lang }, i.Text });
            dbitem.FunctionName.TitleResource.Rows.Should().NotBeEmpty().And.BeEquivalentTo(expected);
        }

        [Theory, AutoModelData]
        public async Task UpdateDescriptionTest(FunctionModel model, DAL.Model.Function dbitem, List<string> newDescr)
        {
            // setup            
            dbitem.Id = model.Id;
            dbitem.FunctionName.SystemName = model.SystemName;
            dbitem.DescriptionResource.Rows.Add(new DAL.Model.TextResourceRow { LanguageId = context.Languages.First().Id, Text = "txt" });
            await context.Functions.AddAsync(dbitem);
            await context.SaveChangesAsync();

            model.Descriptions = newDescr.ToModelTitles(context);

            // action
            await target.UpdateFunctionAsync(model);

            // asserts
            dbitem = context.Functions.Should().ContainSingle(i => i.Id == model.Id).Which;

            dbitem.FunctionName.TitleResource.Should().NotBeNull();
            var expected = model.Descriptions.Select(i => new { Language = new { Code = i.Lang }, i.Text });
            dbitem.DescriptionResource.Rows.Should().NotBeEmpty().And.BeEquivalentTo(expected);
        }

        [Theory]
        [InlineAutoModelData("1.0.1", "1.0.0")]
        [InlineAutoModelData("1.0.1", "1.0.1")]
        [InlineAutoModelData("1.0.1", null)]
        [InlineAutoModelData(null, "1.0.1")]
        public async Task VersionAssignmentTest(string newVersion, string oldVersion, FunctionModel model, DAL.Model.Function dbitem)
        {
            // setup
            dbitem.VersionAssignment = null;

            var dbOldVersion = CreateVersion(oldVersion);
            var dbNewVersion = CreateVersion(newVersion);
            await context.SaveChangesAsync();

            dbitem.Id = model.Id;
            dbitem.FunctionName.SystemName = model.SystemName;

            if (dbOldVersion != null)
                dbitem.VersionAssignment = new DAL.Model.FunctionVersionAssignment { VersionId = dbOldVersion.Id };
            context.Functions.Add(dbitem);
            await context.SaveChangesAsync();
            
            model.LibraryVersion = newVersion;

            // action
            await target.UpdateFunctionAsync(model);

            // asserts
            dbitem = context.Functions.Should().ContainSingle(i => i.Id == model.Id).Which;

            if (newVersion != null)
            {
                dbitem.VersionAssignment.Should().NotBeNull();
                dbitem.VersionAssignment.VersionId.Should().Be(dbNewVersion.Id);
            } 
            else
            {
                dbitem.VersionAssignment.Should().BeNull();
            }

            DAL.Model.Version CreateVersion(string version)
            {
                if (version == null)
                    return null;

                var item = new DAL.Model.Version { VersionValue = version.Split('.').Select(i => int.Parse(i)).ToArray() };
                context.Versions.Add(item);
                return item;
            }
        }

        [Theory, AutoModelData]
        public async Task FunctionNotFound_ShouldBeException(FunctionModel model)
        {
            // setup

            // action
            Func<Task> action = () => target.UpdateFunctionAsync(model);

            // asserts
            await action.Should().ThrowExactlyAsync<FunctionNotFoundException>();
        }

        [Theory, AutoModelData]
        public async Task UnknownLanguageShouldBeException(FunctionModel model, DAL.Model.Function dbitem)
        {
            // setup
            dbitem.Id = model.Id;
            dbitem.FunctionName.SystemName = model.SystemName;
            await context.Functions.AddAsync(dbitem);
            await context.SaveChangesAsync();

            model.Titles = new[] { new TextResource { Lang = "xyz", Text = "title" } };

            // action
            Func<Task> action = () => target.InsertFunctionAsync(model);

            // asserts
            (await action.Should().ThrowExactlyAsync<LanguageNotFoundException>()).Which.LanguageCode.Should().Be("xyz");
        }

        [Theory, AutoModelData]
        public async Task UnknownVersionShouldBeException(FunctionModel model, DAL.Model.Function dbitem)
        {
            // setup
            dbitem.Id = model.Id;
            dbitem.FunctionName.SystemName = model.SystemName;
            await context.Functions.AddAsync(dbitem);
            await context.SaveChangesAsync();

            model.LibraryVersion = "4.5.6";

            // action
            Func<Task> action = () => target.InsertFunctionAsync(model);

            // asserts
            (await action.Should().ThrowExactlyAsync<LibraryVersionNotFoundException>()).Which.Version.Should().Be("4.5.6");
        }

        [Theory, AutoModelData]
        public async Task UpdateFunctionGroupsTest(FunctionModel model, DAL.Model.Function dbitem, List<DAL.Model.FunctionGroup> groups)
        {
            // setup
            await context.FunctionGroups.AddRangeAsync(groups);
            await context.SaveChangesAsync();

            dbitem.Id = model.Id;
            dbitem.FunctionName.SystemName = model.SystemName;
            dbitem.FunctionName.Groups.Add(groups[0]);
            dbitem.FunctionName.Groups.Add(groups[1]);
            await context.Functions.AddAsync(dbitem);
            await context.SaveChangesAsync();

            model.Groups = new List<Guid> { groups[1].Id, groups[2].Id };

            // action
            await target.UpdateFunctionAsync(model);

            // asserts
            dbitem = context.Functions.Should().ContainSingle(i => i.Id == model.Id).Which;

            dbitem.FunctionName.Groups.Select(i => i.Id).Should().BeEquivalentTo(model.Groups);            
        }

        [Theory, AutoModelData]
        public async Task UpdateFunctionWithNullGroups_GroupsShouldBeStayUntouched(FunctionModel model, DAL.Model.Function dbitem, List<DAL.Model.FunctionGroup> groups)
        {
            // setup
            await context.FunctionGroups.AddRangeAsync(groups);
            await context.SaveChangesAsync();

            dbitem.Id = model.Id;
            dbitem.FunctionName.SystemName = model.SystemName;
            dbitem.FunctionName.Groups.AddRange(groups);
            await context.Functions.AddAsync(dbitem);
            await context.SaveChangesAsync();

            model.Groups = null;

            // action
            await target.UpdateFunctionAsync(model);

            // asserts
            dbitem = context.Functions.Should().ContainSingle(i => i.Id == model.Id).Which;

            dbitem.FunctionName.Groups.Select(i => i.Id).Should().BeEquivalentTo(groups.Select(i => i.Id));
        }
    }
}
