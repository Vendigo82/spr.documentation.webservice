﻿using SPR.Documentation.DataContract.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.Services.Tests
{
    public static class TestExtensions
    {
        public static IEnumerable<TextResource> ToModelTitles(this IList<string> texts, DAL.DocContext context)
        {
            int index = 0;
            return context.Languages.ToList().Select(i => new TextResource { Lang = i.Code, Text = texts[index++] }).ToArray();
        }
    }
}
