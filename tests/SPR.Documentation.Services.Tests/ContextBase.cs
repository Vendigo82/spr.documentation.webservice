﻿using Microsoft.Extensions.Configuration;
using SPR.Documentation.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.Services
{
    public class ContextBase
    {
        protected DocContext context;

        public ContextBase()
        {
            context = Tools.InitMemoryDd();
            context.Languages.Add(new DAL.Model.Language { Code = "en", Title = "English" });
            context.Languages.Add(new DAL.Model.Language { Code = "ru", Title = "Russian" });
            context.Languages.Add(new DAL.Model.Language { Code = "cn", Title = "Chinese" });
            context.SaveChanges();
        }
    }
}
