﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SPR.Documentation.WebService.Filters;
using System.Net.Mime;

namespace SPR.Documentation.WebService.Controllers
{
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ProducesErrorResponseType(typeof(ProblemDetails))]
    [PostgresExceptionsFilter]
    [EditorExceptionsFilter]
    [Authorize(Policy = AuthPolicy.EditorPolicy)]
    public class EditorControllerBase : ControllerBase
    {
    }
}
