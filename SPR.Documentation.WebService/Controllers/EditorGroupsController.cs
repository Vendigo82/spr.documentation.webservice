﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SPR.Documentation.DataContract;
using SPR.Documentation.DataContract.Editor;
using SPR.Documentation.Services.Abstractions;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Threading.Tasks;

namespace SPR.Documentation.WebService.Controllers
{
    [Route("editor/groups")]
    public class EditorGroupsController : EditorControllerBase
    {
        private readonly IGroupsEditorService service;

        public EditorGroupsController(IGroupsEditorService service)
        {
            this.service = service ?? throw new ArgumentNullException(nameof(service));
        }

        /// <summary>
        /// Returns list of all function's groups
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(ItemsContainer<GroupBriefModel>))]
        public async Task<IActionResult> GetListAsync()
        {
            var result = await service.GetListAsync();
            return Ok(new ItemsContainer<GroupBriefModel> { Items = result });
        }

        /// <summary>
        /// Get function's group by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = nameof(GetItemAsync))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(GroupModel))]
        [SwaggerResponse(StatusCodes.Status204NoContent, Description = "Group with given id does not found")]
        public async Task<IActionResult> GetItemAsync(Guid id)
        {
            var result = await service.GetItemAsync(id);
            return Ok(result);
        }

        /// <summary>
        /// Create new group
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [SwaggerResponse(StatusCodes.Status201Created, Type = typeof(IdModel), Description = "Group successfully created")]
        [SwaggerResponse(StatusCodes.Status409Conflict, Description = "Group with given id or alias already existed")]
        [SwaggerResponse(StatusCodes.Status422UnprocessableEntity, Type = typeof(ValidationProblemDetails), Description = "Can't create group with given model")]
        [SwaggerResponseExample(StatusCodes.Status409Conflict, typeof(SwaggerExamples.GroupCreateConflictExamples))]
        [SwaggerResponseHeader(StatusCodes.Status201Created, "Location", "string", "Location of the newly created resource")]
        public async Task<IActionResult> PutItemAsync([FromBody] GroupModel model)
        {
            var id = await service.InsertAsync(model);
            return CreatedAtRoute(nameof(GetItemAsync), new { id }, new IdModel { Id = id });
        }

        /// <summary>
        /// Update group by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("{id}")]
        [SwaggerResponse(StatusCodes.Status200OK, Description = "Group successfully updated")]
        [SwaggerResponse(StatusCodes.Status404NotFound, Description = "Group does not found")]
        [SwaggerResponse(StatusCodes.Status409Conflict, Description = "Group with given alias already existed")]
        [SwaggerResponse(StatusCodes.Status422UnprocessableEntity, Type = typeof(ValidationProblemDetails), Description = "Can't create group with given model")]
        [SwaggerResponseExample(StatusCodes.Status404NotFound, typeof(SwaggerExamples.GroupNotFoundExamples))]
        [SwaggerResponseExample(StatusCodes.Status409Conflict, typeof(SwaggerExamples.GroupUpdateConflictExamples))]
        public async Task<IActionResult> PostItemAsync(Guid id, [FromBody] GroupModel model)
        {
            if (model.Id != id)
            {
                ModelState.AddModelError("id", "Model's id and route id does not match");
                return UnprocessableEntity(new ValidationProblemDetails(ModelState));
            }

            await service.UpdateAsync(model);
            return Ok();
        }
    }
}
