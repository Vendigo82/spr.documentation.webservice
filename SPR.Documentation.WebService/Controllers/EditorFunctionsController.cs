﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SPR.Documentation.DataContract;
using SPR.Documentation.DataContract.Editor;
using SPR.Documentation.Services.Abstractions;
using SPR.Documentation.Services.Exceptions;
using SPR.Documentation.WebService.Extensions;
using SPR.Documentation.WebService.Filters;
using SPR.Documentation.WebService.Settings;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace SPR.Documentation.WebService.Controllers
{
    [Route("editor/functions")]
    public class EditorFunctionsController : EditorControllerBase
    {
        private readonly IFunctionsEditorService _service;
        private readonly ITokenConvertor<FunctionBriefModel> _tokenConvertor;
        private readonly IOptions<EditorControllerSettings> _options;

        public EditorFunctionsController(IFunctionsEditorService service, ITokenConvertor<FunctionBriefModel> tokenConvertor, IOptions<EditorControllerSettings> options)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _tokenConvertor = tokenConvertor ?? throw new ArgumentNullException(nameof(tokenConvertor));
            _options = options ?? throw new ArgumentNullException(nameof(options));
        }

        /// <summary>
        /// Returns list of functions
        /// </summary>
        /// <param name="filter">functions filter (by system name)</param>
        /// <param name="maxCount">maximum count of functions in response</param>
        /// <param name="offset">data offset</param>
        /// <param name="token">requesting next partition token from previous request</param>
        /// <returns></returns>
        [HttpGet()]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(PartialItemsContainer<FunctionBriefModel>))]
        public async Task<IActionResult> GetFunctionsListAsync(
            [FromQuery] string? filter = null,
            [FromQuery] int? offset = null,
            [FromQuery, Range(1, int.MaxValue)] int? maxCount = null,
            [FromQuery] string? token = null)
        {
            try
            {
                var options = new Services.Model.FunctionsRequestOptions() {
                    Filter = filter,
                    MaxCount = Math.Min(_options.Value.MaxFunctionsCount, maxCount ?? _options.Value.MaxFunctionsCount),
                    StartsFrom = DeserializeToken(),
                    Offset = offset
                };

                var list = await _service.GetFunctionsListAsync(options);
                var response = new PartialItemsContainer<FunctionBriefModel> {
                    Items = list,
                    NextToken = SerializeToken(),
                    Exhausted = list.Count() < options.MaxCount
                };
                return Ok(response);

                FunctionBriefModel? DeserializeToken()
                {
                    if (token == null)
                        return null;

                    return _tokenConvertor.Deserialize(token);
                }

                string? SerializeToken()
                {
                    var last = list.LastOrDefault();
                    if (last == null)
                        return token;

                    return _tokenConvertor.Serialize(last);
                }

            }
            catch (InvalidTokenException e)
            {
                ModelState.AddModelError(nameof(token), e.Message);
                return BadRequest(new ValidationProblemDetails(ModelState));
            }
        }

        /// <summary>
        /// Get count of functions
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet("count")]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(CountResponseModel))]
        public async Task<IActionResult> GetFunctionsCountAsync([FromQuery] string? filter = null)
        {
            var count = await _service.GetFunctionsCountAsync(filter);
            return Ok(new CountResponseModel { Count = count });
        }

        /// <summary>
        /// Get function by id
        /// </summary>
        /// <param name="id">function's id</param>
        /// <returns></returns>
        [HttpGet("{id}", Name = nameof(GetFunctionAsync))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(FunctionModel))]
        [SwaggerResponse(StatusCodes.Status204NoContent, Description = "Function with given id does not found")]        
        public async Task<IActionResult> GetFunctionAsync(Guid id)
        {
            var response = await _service.GetFunctionAsync(id);
            if (response == null)
                return NoContent();

            return Ok(response);
        }

        /// <summary>
        /// Insert new function
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost()]
        [HttpPut]
        //[EditorExceptionsFilter]
        [SwaggerResponse(StatusCodes.Status201Created, Type = typeof(IdModel), Description = "Function successfully created")]
        [SwaggerResponse(StatusCodes.Status409Conflict, Description = "Create function conflict")]
        [SwaggerResponse(StatusCodes.Status422UnprocessableEntity, Type = typeof(ValidationProblemDetails), Description = "Input data model error")]
        [SwaggerResponseExample(StatusCodes.Status409Conflict, typeof(SwaggerExamples.FunctionCreateConflictExamples))]
        [SwaggerResponseHeader(StatusCodes.Status201Created, "Location", "string", "Location of the newly created resource")]
        public async Task<IActionResult> InsertFunctionAsync([FromBody] FunctionModel model)
        {
            try
            {
                var id = await _service.InsertFunctionAsync(model);
                return CreatedAtRoute(nameof(GetFunctionAsync), new { id }, new IdModel { Id = id });
            } 
            catch (GroupNotFoundException e)
            {
                return TreatGroupNotFoundException(model, e);
            }
        }

        /// <summary>
        /// Update function
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <response code="422">Input data model error</response>
        [HttpPost("{id}")]
        //[EditorExceptionsFilter]
        [SwaggerResponse(StatusCodes.Status200OK)]
        [SwaggerResponse(StatusCodes.Status404NotFound, Description = "Function with given id does not found")]
        [SwaggerResponse(StatusCodes.Status409Conflict, Description = "Function update conflict")]
        [SwaggerResponse(StatusCodes.Status422UnprocessableEntity, Type = typeof(ValidationProblemDetails), Description = "Input data model error")]
        [SwaggerResponseExample(StatusCodes.Status409Conflict, typeof(SwaggerExamples.FunctionUpdateConflictExamples))]
        [SwaggerResponseExample(StatusCodes.Status404NotFound, typeof(SwaggerExamples.FunctionNotFoundExamples))]
        public async Task<IActionResult> UpdateFunctionAsync([FromRoute] Guid id, [FromBody] FunctionModel model)
        {
            if (model.Id != id)
            {
                ModelState.AddModelError("id", "Model's id and route id does not match");
                return UnprocessableEntity(new ValidationProblemDetails(ModelState));
            }

            try
            {
                await _service.UpdateFunctionAsync(model);
                return Ok();
            }
            catch (GroupNotFoundException e)
            {
                return TreatGroupNotFoundException(model, e);
            }
        }

        private IActionResult TreatGroupNotFoundException(FunctionModel model, GroupNotFoundException e)
        {
            if (model.Groups == null)
                throw e;

            foreach (var groupId in e.GroupsIds)
            {
                var index = model.Groups.IndexOf(groupId);
                if (index != -1)
                    ModelState.AddModelError($"$.{nameof(model.Groups)}[{index}]", "Group with given id does not found");
                else
                    throw e;
            }
            return UnprocessableEntity(new ValidationProblemDetails(ModelState));
        }
    }
}
