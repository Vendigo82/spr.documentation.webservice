﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SPR.Documentation.DataContract;
using SPR.Documentation.DataContract.Editor;
using SPR.Documentation.Services.Abstractions;
using SPR.Documentation.Services.Exceptions;
using SPR.Documentation.WebService.Filters;
using SPR.Documentation.WebService.Settings;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace SPR.Documentation.WebService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ProducesErrorResponseType(typeof(ProblemDetails))]
    [PostgresExceptionsFilter]
    [Authorize(Policy = AuthPolicy.EditorPolicy)]
    public class EditorController : ControllerBase
    { 
        private readonly IFunctionsEditorService _service;

        public EditorController(IFunctionsEditorService service)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        /// <summary>
        /// Returns list of available data types
        /// </summary>
        /// <returns></returns>
        [HttpGet("datatypes")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ItemsContainer<string>))]
        public async Task<IActionResult> GetDataTypesAsync()
        {
            var list = await _service.GetDataTypesAsync();
            return Ok(new ItemsContainer<string> { Items = list });
        }
    }
}
