﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace SPR.Documentation.WebService.Controllers
{
    [Route("[controller]")]
    [ApiController]    
    public class InfoController : ControllerBase
    {
        [HttpGet("/version")]
        [AllowAnonymous]
        public IActionResult Version()
        {
            var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version!.ToString();
            return Ok(new { Version = version });
        }
    }
}
