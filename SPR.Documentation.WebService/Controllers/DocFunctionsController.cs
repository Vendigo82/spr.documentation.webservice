﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SPR.Documentation.DataContract;
using SPR.Documentation.DataContract.Doc;
using SPR.Documentation.Services.Abstractions;
using SPR.Documentation.WebService.Filters;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace SPR.Documentation.WebService.Controllers
{
    [Route("doc/{project}/{version}/{language}")]
    [ApiController()]
    [AllowAnonymous]
    [DocExceptionFilter]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ProducesErrorResponseType(typeof(ProblemDetails))]

    [SwaggerResponse(StatusCodes.Status404NotFound, Description = "Project not found")]
    [SwaggerResponseExample(StatusCodes.Status404NotFound, typeof(SwaggerExamples.DocProjectNotFoundExamples))]

    public class DocFunctionsController : ControllerBase
    {
        private readonly IFunctionsDocService functionsService;        

        public DocFunctionsController(IFunctionsDocService functionsService)
        {
            this.functionsService = functionsService ?? throw new ArgumentNullException(nameof(functionsService));
        }

        [FromRoute(Name = "language")]
        public string Language { get; set; } = null!;

        [FromRoute(Name = "project")]
        public string Project { get; set; } = null!;

        [FromRoute(Name = "version")]
        public string Version { get; set; } = null!;

        /// <summary>
        /// Get all functions with filter
        /// </summary>
        /// <param name="groupFilter">Filter by group</param>
        /// <param name="nameFilter">Filter by name</param>
        /// <returns></returns>
        [HttpGet("functions")]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(DataContract.ItemsContainer<DataContract.Doc.FunctionBriefDocModel>))]
        public async Task<IActionResult> GetFunctionsListAsync(string? groupFilter, string? nameFilter)
        {
            var items = await functionsService.GetFunctionsListAsync(MakePresentationRequest(), groupFilter, nameFilter);
            return Ok(new ItemsContainer<FunctionBriefDocModel> { Items = items });
        }

        /// <summary>
        /// Get function's info
        /// </summary>
        /// <param name="systemName">function's system name</param>
        /// <returns></returns>
        [HttpGet("functions/{systemName}")]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(DataContract.Doc.FunctionDocModel))]
        [SwaggerResponse(StatusCodes.Status204NoContent, Description = "Function not found")]
        public async Task<IActionResult> GetFunctionAsync(string systemName)
        {
            var item = await functionsService.GetFunctionAsync(MakePresentationRequest(), systemName);
            if (item == null)
                return NoContent();

            return Ok(item);
        }

        /// <summary>
        /// Get list of groups
        /// </summary>
        /// <returns></returns>
        [HttpGet("groups")]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(ItemsContainer<GroupDocModel>))]
        public async Task<IActionResult> GetGroupsAsync()
        {
            var items = await functionsService.GetGroupsListAsync(MakePresentationRequest());
            return Ok(new ItemsContainer<GroupDocModel> { Items = items });
        }

        private PresentationRequest MakePresentationRequest()
        {
            var versionArray = Version.Split('.').Select(i => int.Parse(i)).ToArray();
            return new PresentationRequest(Language, Project, versionArray);
        }
    }
}
