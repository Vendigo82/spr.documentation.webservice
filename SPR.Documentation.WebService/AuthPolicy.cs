﻿using IdentityModel;
using Microsoft.AspNetCore.Authorization;

namespace SPR.Documentation.WebService
{
    public static class AuthPolicy
    {
        public const string EditorPolicy = nameof(EditorPolicy);
    }

    public static class AuthorizationOptionsExtensions
    {
        private const string EditorScope = "sprdoc.editor";

        public static void ConfigurePolicies(this AuthorizationOptions options, string schemeName)
        {
            options.AddPolicy(AuthPolicy.EditorPolicy, policy => {
                policy.AddAuthenticationSchemes(schemeName);
                policy.RequireClaim(JwtClaimTypes.Scope, EditorScope);
            });
        }

    }
}
