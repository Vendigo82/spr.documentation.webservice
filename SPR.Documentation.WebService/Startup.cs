using FluentValidation.AspNetCore;
using HealthChecks.UI.Client;
using Hellang.Middleware.ProblemDetails;
using MicroElements.Swashbuckle.FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Prometheus;
using SPR.Documentation.DAL;
using SPR.Documentation.Services;
using SPR.Documentation.Services.Abstractions;
using SPR.Documentation.WebService.Settings;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SPR.Documentation.WebService
{
    public class Startup
    {
        private const string DeepTag = "deep";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddControllers()
                .AddFluentValidation(c => c.RegisterValidatorsFromAssemblyContaining<Startup>());

            services.AddProblemDetails();

            services.AddDbContext<DocContext>(options => options.UseNpgsql(Configuration.GetConnectionString("Default")));

            services.AddAutoMapper(typeof(Services.Mapping.MappingProfile));
            services.AddTransient<ITokenConvertor<DataContract.Editor.FunctionBriefModel>, FunctionTokenConverter>();
            services.AddTransient<IFunctionsEditorService, FunctionsEditorService>();
            services.AddTransient<IGroupsEditorService, GroupsEditorService>();

            services.AddTransient<IFunctionsDocService, FunctionsDocService>();

            var identityServerCfg = Configuration.GetSection("IdentityServer").Get<IdentityServerSettings>();
            services.Configure<FunctionsDocService.Settings>(Configuration.GetSection("Documentation"));

            AddHealthCheck();
            AddIdentity();
            AddSwagger();

            void AddIdentity()
            {                
                services
                    .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options => {
                        options.Authority = identityServerCfg.BaseUrl ?? throw new Exception($"Missing configufration value 'IdentityServer:{nameof(identityServerCfg.BaseUrl)}'");
                        options.TokenValidationParameters.ValidIssuer = identityServerCfg.Issuer ?? identityServerCfg.BaseUrl;
                        options.RequireHttpsMetadata = identityServerCfg.RequireHttpsMetadata;
                        options.Audience = identityServerCfg.Audience;
                        options.TokenValidationParameters.ValidateIssuer = true;
                        options.TokenValidationParameters.ValidateLifetime = true;
                        options.TokenValidationParameters.ClockSkew = TimeSpan.FromSeconds(30);                        
                    });

                services.AddAuthorization(options => options.ConfigurePolicies(JwtBearerDefaults.AuthenticationScheme));
            }

            void AddHealthCheck()
            {
                services.AddHealthChecks()
                    .AddDbContextCheck<DocContext>("Database", tags: new[] { "database" })
                    .AddIdentityServer(new Uri(identityServerCfg.BaseUrl), "IdentityServer", tags: new[] { DeepTag })
                    .ForwardToPrometheus()
                    ;
            }

            void AddSwagger()
            {
                services.AddSwaggerGen(c => {
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "SPR.Documentation.WebService", Version = "v1" });

                    c.SupportNonNullableReferenceTypes();
                    c.UseAllOfToExtendReferenceSchemas();

                    c.ExampleFilters();
                    c.EnableAnnotations();

                    c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme {
                        Description = "Standard Authorization header using the Bearer scheme. Example: \"bearer {token}\"",
                        In = ParameterLocation.Header,
                        Name = "Authorization",
                        Type = SecuritySchemeType.ApiKey
                    });

                    // add Security information to each operation for OAuth2
                    c.OperationFilter<SecurityRequirementsOperationFilter>(false);

                    // Set the comments path for the Swagger JSON and UI.
                    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                    c.IncludeXmlComments(xmlPath);

                    var apiXmlFile = $"{typeof(DataContract.ItemsContainer<int>).Assembly.GetName().Name}.xml";
                    c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, apiXmlFile));
                });

                services.AddFluentValidationRulesToSwagger();
                services.AddFluentValidationRulesToSwagger(options => {
                    options.SetNotNullableIfMinLengthGreaterThenZero = true;
                    options.UseAllOffForMultipleRules = true;
                });

                services.AddSwaggerExamplesFromAssemblyOf<Startup>();
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseProblemDetails();

            //var enableMigrations = Configuration.GetValue<bool>("EnableMigrations");
            //if (env.IsDevelopment() || enableMigrations)
            //    InitializeDatabase(app);

            if (env.IsDevelopment())
            {
                //app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "SPR.Documentation.WebService v1"));
            }

            app.UseHealthChecks("/health", new HealthCheckOptions 
            {
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            });
            app.UseHealthChecks("/health/lite", new HealthCheckOptions
            {
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse,
                Predicate = (p) => !p.Tags.Contains(DeepTag)
            });

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseHttpMetrics();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers().RequireAuthorization();
                endpoints.MapMetrics();
            });
        }

        private static void InitializeDatabase(IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
            var context = serviceScope.ServiceProvider.GetRequiredService<DocContext>();
            context.Database.Migrate();
            //serviceScope.ServiceProvider.GetRequiredService<UsersContext>().Database.Migrate();
        }

    }
}
