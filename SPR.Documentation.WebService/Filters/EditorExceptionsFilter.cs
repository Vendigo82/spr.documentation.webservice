﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using SPR.Documentation.Services.Exceptions;
using System;

namespace SPR.Documentation.WebService.Filters
{
    public class EditorExceptionsFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            var result = TreatException(context.Exception);
            if (result != null)
            {
                context.Result = result;
                context.ExceptionHandled = true;
            }
            
            IActionResult? TreatException(Exception exception)
            {
                switch (exception)
                {
                    case LanguageNotFoundException e:
                        {
                            context.ModelState.AddModelError("language", e.Message);
                            var payload = new ValidationProblemDetails(context.ModelState);
                            payload.Extensions["language"] = e.LanguageCode;
                            return new UnprocessableEntityObjectResult(payload);
                        }

                    case LibraryVersionNotFoundException e:
                        {
                            context.ModelState.AddModelError(nameof(DataContract.Editor.FunctionModel.LibraryVersion), e.Message);
                            var payload = new ValidationProblemDetails(context.ModelState);
                            return new UnprocessableEntityObjectResult(payload);
                        }

                    case GroupNotFoundException e:
                        return new NotFoundObjectResult(Factory.GroupNotFound(e));

                    case FunctionNotFoundException e:
                        return new NotFoundObjectResult(Factory.FunctionNotFound(e));

                    default:
                        return null;
                }
            }
        }

        public static class Factory
        {
            public static ProblemDetails GroupNotFound(GroupNotFoundException e) => ObjectNotFound("Group", e.Message);
            public static ProblemDetails FunctionNotFound(FunctionNotFoundException e) => ObjectNotFound("Function", e.Message);

            private static ProblemDetails ObjectNotFound(string objectName, string message) => new ProblemDetails {
                Title = $"{objectName} does not exist",
                Detail = message
            };
        }
    }

    public class EditorExceptionsFilterAttribute : TypeFilterAttribute
    {
        public EditorExceptionsFilterAttribute() : base(typeof(EditorExceptionsFilter))
        {
        }
    }
}
