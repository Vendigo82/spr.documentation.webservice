﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using SPR.Documentation.Services.Exceptions;
using System;

namespace SPR.Documentation.WebService.Filters
{
    public class DocExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            var result = TreatException(context.Exception);
            if (result != null)
            {
                context.Result = result;
                context.ExceptionHandled = true;
            }

            static IActionResult? TreatException(Exception exception) => exception switch
            {
                ProjectNotFoundException e => new NotFoundObjectResult(Factory.ProjectNotFound(e.ProjectName)),
                _ => null,
            };            
        }

        public static class Factory
        {
            public static ProblemDetails ProjectNotFound(string projectName) => new()
            { 
                Title = "project not found", 
                Detail = $"Project '{projectName}' does not found" 
            };
        }
    }

    public class DocExceptionFilterAttribute : TypeFilterAttribute
    {
        public DocExceptionFilterAttribute() : base(typeof(DocExceptionFilter))
        {
        }
    }
}
