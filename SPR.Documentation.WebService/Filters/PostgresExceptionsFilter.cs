﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;

namespace SPR.Documentation.WebService.Filters
{
    public class PostgresExceptionsFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            if (context.Exception is DbUpdateException dbe && dbe.InnerException is Npgsql.PostgresException exception)
            {
                var result = Treat(exception);
                if (result != null)
                {
                    context.Result = result;
                    context.ExceptionHandled = true;
                }
            }

            static IActionResult? Treat(Npgsql.PostgresException exception) => exception.SqlState switch {
                Npgsql.PostgresErrorCodes.UniqueViolation when exception.ConstraintName == DAL.DocContext.FunctionUniqueKey => 
                    new ConflictObjectResult(Factory.FunctionSignatureConflict()),

                Npgsql.PostgresErrorCodes.UniqueViolation when exception.ConstraintName == DAL.DocContext.GroupUniqueKey =>
                    new ConflictObjectResult(Factory.GroupAliasConflict()),

                Npgsql.PostgresErrorCodes.UniqueViolation when exception.ConstraintName!.Contains("pkey") || exception.ConstraintName!.StartsWith("pk_") => 
                    new ConflictObjectResult(Factory.PrimaryKeyConflict(exception.ConstraintName)),

                _ => null,
            };
        }

        public static class Factory
        {
            public static ProblemDetails FunctionSignatureConflict() => new() { Title = "Function already exists", Detail = "Function with given name and args count already exists" };
            public static ProblemDetails GroupAliasConflict() => new() { Title = "Group already exists", Detail = "Group with given alias already exists" };
            public static ProblemDetails PrimaryKeyConflict(string constraint) => new() { Title = "Primary key conflict", Detail = $"Primary key '{constraint}' violation" };
        }
    }

    public class PostgresExceptionsFilterAttribute : TypeFilterAttribute
    {
        public PostgresExceptionsFilterAttribute() : base(typeof(PostgresExceptionsFilter))
        {
        }
    }
}
