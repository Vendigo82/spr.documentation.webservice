﻿using System.ComponentModel.DataAnnotations;

namespace SPR.Documentation.WebService.Settings
{
    public class IdentityServerSettings
    {        
        [Required]
        public string BaseUrl { get; set; } = null!;

        public string? Issuer { get; set; }

        public string Audience { get; set; } = "sprdoc";

        public bool RequireHttpsMetadata { get; set; } = true;
    }
}
