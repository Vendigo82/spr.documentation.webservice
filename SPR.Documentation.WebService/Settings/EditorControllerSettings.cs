﻿namespace SPR.Documentation.WebService.Settings
{
    public class EditorControllerSettings
    {
        public int MaxFunctionsCount { get; set; } = 50;
    }
}
