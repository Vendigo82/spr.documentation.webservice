﻿using FluentValidation;
using SPR.Documentation.DataContract.Editor;

namespace SPR.Documentation.WebService.ModelValidation
{
    public class ArgumentModelValidator : AbstractValidator<ArgumentModel>
    {
        public ArgumentModelValidator()
        {
            RuleFor(c => c.Name).NotNull().NotEmpty();
            RuleFor(c => c.Type).NotNull().NotEmpty();
        }
    }
}
