﻿using FluentValidation;
using SPR.Documentation.DataContract.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.Documentation.WebService.ModelValidation
{
    public class GroupModelValidator : AbstractValidator<GroupModel>
    {
        public GroupModelValidator()
        {
            RuleFor(p => p.Alias).NotEmpty();
            RuleFor(p => p.Titles).NotEmpty();
        }
    }
}
