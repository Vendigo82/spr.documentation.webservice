﻿using FluentValidation;
using SPR.Documentation.DataContract.Editor;

namespace SPR.Documentation.WebService.ModelValidation
{
    public class ReturnValueModelValidator : AbstractValidator<ReturnValueModel>
    {
        public ReturnValueModelValidator()
        {
            RuleFor(p => p.Type).NotNull().NotEmpty();
        }
    }
}
