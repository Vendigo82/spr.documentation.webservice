﻿using FluentValidation;
using SPR.Documentation.DataContract.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.Documentation.WebService.ModelValidation
{
    public class FunctionModelValidator : AbstractValidator<FunctionModel>
    {
        public FunctionModelValidator()
        {
            RuleFor(p => p.ArgsCount).GreaterThanOrEqualTo((short)0);
            RuleFor(p => new { p.ArgsCount, p.Arguments }).Must(p => p.ArgsCount == p.Arguments.Count()).WithMessage("Arguments count not match");
            RuleFor(p => p.SystemName).NotEmpty();
        }
    }
}
