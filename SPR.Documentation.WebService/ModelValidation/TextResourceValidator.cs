﻿using FluentValidation;
using SPR.Documentation.DataContract.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.Documentation.WebService.ModelValidation
{
    public class TextResourceValidator : AbstractValidator<TextResource>
    {
        public TextResourceValidator()
        {
            RuleFor(p => p.Lang).NotEmpty();
            RuleFor(p => p.Text).NotEmpty();
        }
    }
}
