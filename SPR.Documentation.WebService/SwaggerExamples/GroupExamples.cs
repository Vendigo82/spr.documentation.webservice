﻿using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;

namespace SPR.Documentation.WebService.SwaggerExamples
{
    public class GroupCreateConflictExamples : IMultipleExamplesProvider<object>
    {
        public IEnumerable<SwaggerExample<object>> GetExamples()
        {
            yield return SwaggerExample.Create("Alias conflict", (object)Filters.PostgresExceptionsFilter.Factory.GroupAliasConflict());
            yield return SwaggerExample.Create("Primary key conflict", (object)Filters.PostgresExceptionsFilter.Factory.PrimaryKeyConflict("constraint_name"));
        }
    }

    public class GroupUpdateConflictExamples : IExamplesProvider<object>
    {
        public object GetExamples() => Filters.PostgresExceptionsFilter.Factory.GroupAliasConflict();
    }

    public  class GroupNotFoundExamples : IExamplesProvider<object>
    {
        public object GetExamples() => Filters.EditorExceptionsFilter.Factory.GroupNotFound(new Services.Exceptions.GroupNotFoundException(Guid.Empty));
    }
}
