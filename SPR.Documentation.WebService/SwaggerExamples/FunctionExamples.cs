﻿using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.Documentation.WebService.SwaggerExamples
{
    public class FunctionCreateConflictExamples : IMultipleExamplesProvider<object>
    {
        public IEnumerable<SwaggerExample<object>> GetExamples()
        {
            yield return SwaggerExample.Create("Function signature conflict", (object)Filters.PostgresExceptionsFilter.Factory.FunctionSignatureConflict());
            yield return SwaggerExample.Create("Primary key conflict", (object)Filters.PostgresExceptionsFilter.Factory.PrimaryKeyConflict("constraint_name"));
        }
    }

    public class FunctionUpdateConflictExamples : IExamplesProvider<object>
    {
        public object GetExamples() => Filters.PostgresExceptionsFilter.Factory.FunctionSignatureConflict();
    }

    public class FunctionNotFoundExamples : IExamplesProvider<object>
    {
        public object GetExamples() => Filters.EditorExceptionsFilter.Factory.FunctionNotFound(new Services.Exceptions.FunctionNotFoundException(Guid.Empty));
    }
}
