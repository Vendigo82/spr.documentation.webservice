﻿using Swashbuckle.AspNetCore.Filters;

namespace SPR.Documentation.WebService.SwaggerExamples
{
    public class DocProjectNotFoundExamples : IExamplesProvider<object>
    {
        public object GetExamples() => Filters.DocExceptionFilter.Factory.ProjectNotFound("project");
    }
}
