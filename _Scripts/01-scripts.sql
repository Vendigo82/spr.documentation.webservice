﻿CREATE TABLE IF NOT EXISTS "__EFMigrationsHistory" (
    migration_id character varying(150) NOT NULL,
    product_version character varying(32) NOT NULL,
    CONSTRAINT pk___ef_migrations_history PRIMARY KEY (migration_id)
);

START TRANSACTION;


DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE SCHEMA IF NOT EXISTS doc;
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE SCHEMA IF NOT EXISTS text;
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE SCHEMA IF NOT EXISTS project;
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE EXTENSION IF NOT EXISTS pgcrypto;
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE TABLE text.language (
        id uuid NOT NULL DEFAULT (gen_random_uuid()),
        code text NOT NULL,
        title text NOT NULL,
        CONSTRAINT pk_language PRIMARY KEY (id)
    );
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE TABLE text.text_resource (
        id uuid NOT NULL DEFAULT (gen_random_uuid()),
        CONSTRAINT pk_text_resource PRIMARY KEY (id)
    );
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE TABLE doc.version (
        id uuid NOT NULL DEFAULT (gen_random_uuid()),
        version_value integer[] NOT NULL,
        created_at timestamp with time zone NOT NULL DEFAULT (timezone('UTC'::text, now())),
        CONSTRAINT pk_version PRIMARY KEY (id)
    );
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE TABLE doc.function_group (
        id uuid NOT NULL,
        title_resource_id uuid NOT NULL,
        CONSTRAINT pk_function_group PRIMARY KEY (id),
        CONSTRAINT fk_function_group_text_resource_title_resource_id FOREIGN KEY (title_resource_id) REFERENCES text.text_resource (id)
    );
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE TABLE doc.function_name (
        id uuid NOT NULL DEFAULT (gen_random_uuid()),
        system_name text NOT NULL,
        title_resource_id uuid NOT NULL,
        CONSTRAINT pk_function_name PRIMARY KEY (id),
        CONSTRAINT fk_function_name_text_resource_title_resource_id FOREIGN KEY (title_resource_id) REFERENCES text.text_resource (id)
    );
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE TABLE project.project (
        id uuid NOT NULL DEFAULT (gen_random_uuid()),
        system_name text NOT NULL,
        title_resource_id uuid NOT NULL,
        CONSTRAINT pk_project PRIMARY KEY (id),
        CONSTRAINT fk_project_text_resource_title_resource_id FOREIGN KEY (title_resource_id) REFERENCES text.text_resource (id)
    );
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE TABLE text.text_resource_row (
        text_resource_id uuid NOT NULL,
        language_id uuid NOT NULL,
        text text NOT NULL,
        CONSTRAINT pk_text_resource_row PRIMARY KEY (language_id, text_resource_id),
        CONSTRAINT fk_text_resource_row_language_language_id FOREIGN KEY (language_id) REFERENCES text.language (id),
        CONSTRAINT fk_text_resource_row_text_resource_text_resource_id FOREIGN KEY (text_resource_id) REFERENCES text.text_resource (id) ON DELETE CASCADE
    );
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE TABLE doc.function (
        id uuid NOT NULL DEFAULT (gen_random_uuid()),
        function_name_id uuid NOT NULL,
        args_count smallint NOT NULL,
        arguments jsonb NOT NULL,
        return jsonb NULL,
        description_resource_id uuid NOT NULL,
        CONSTRAINT pk_function PRIMARY KEY (id),
        CONSTRAINT fk_function_function_name_function_name_id FOREIGN KEY (function_name_id) REFERENCES doc.function_name (id) ON DELETE CASCADE,
        CONSTRAINT fk_function_text_resource_description_resource_id FOREIGN KEY (description_resource_id) REFERENCES text.text_resource (id)
    );
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE TABLE doc.function_group_function_name (
        functions_id uuid NOT NULL,
        groups_id uuid NOT NULL,
        CONSTRAINT pk_function_group_function_name PRIMARY KEY (functions_id, groups_id),
        CONSTRAINT fk_function_group_function_name_function_groups_groups_id FOREIGN KEY (groups_id) REFERENCES doc.function_group (id) ON DELETE CASCADE,
        CONSTRAINT fk_function_group_function_name_function_names_functions_id FOREIGN KEY (functions_id) REFERENCES doc.function_name (id) ON DELETE CASCADE
    );
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE TABLE project.project_version (
        id uuid NOT NULL DEFAULT (gen_random_uuid()),
        project_id uuid NOT NULL,
        version integer[] NOT NULL,
        library_version integer[] NOT NULL,
        CONSTRAINT pk_project_version PRIMARY KEY (id),
        CONSTRAINT fk_project_version_project_project_id FOREIGN KEY (project_id) REFERENCES project.project (id) ON DELETE CASCADE
    );
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE TABLE doc.example (
        id uuid NOT NULL DEFAULT (gen_random_uuid()),
        function_id uuid NOT NULL,
        example text NOT NULL,
        "order" integer NOT NULL,
        CONSTRAINT pk_example PRIMARY KEY (id),
        CONSTRAINT fk_example_function_function_id FOREIGN KEY (function_id) REFERENCES doc.function (id) ON DELETE CASCADE
    );
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE TABLE doc.function_version_assignment (
        function_id uuid NOT NULL,
        version_id uuid NOT NULL,
        CONSTRAINT pk_function_version_assignment PRIMARY KEY (function_id, version_id),
        CONSTRAINT fk_function_version_assignment_functions_function_id FOREIGN KEY (function_id) REFERENCES doc.function (id) ON DELETE CASCADE,
        CONSTRAINT fk_function_version_assignment_versions_version_id FOREIGN KEY (version_id) REFERENCES doc.version (id) ON DELETE CASCADE
    );
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE INDEX ix_example_function_id ON doc.example (function_id);
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE INDEX ix_function_description_resource_id ON doc.function (description_resource_id);
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE INDEX ix_function_function_name_id ON doc.function (function_name_id);
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE UNIQUE INDEX unq_function_args_count_function_name_id ON doc.function (args_count, function_name_id);
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE INDEX ix_function_group_title_resource_id ON doc.function_group (title_resource_id);
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE INDEX ix_function_group_function_name_groups_id ON doc.function_group_function_name (groups_id);
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE UNIQUE INDEX ix_function_name_system_name ON doc.function_name (system_name);
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE INDEX ix_function_name_title_resource_id ON doc.function_name (title_resource_id);
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE UNIQUE INDEX ix_function_version_assignment_function_id ON doc.function_version_assignment (function_id);
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE INDEX ix_function_version_assignment_version_id ON doc.function_version_assignment (version_id);
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE UNIQUE INDEX ix_language_code ON text.language (code);
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE UNIQUE INDEX ix_project_system_name ON project.project (system_name);
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE INDEX ix_project_title_resource_id ON project.project (title_resource_id);
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE INDEX ix_project_version_project_id ON project.project_version (project_id);
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE INDEX ix_project_version_version ON project.project_version (version);
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE INDEX ix_text_resource_row_text_resource_id ON text.text_resource_row (text_resource_id);
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    CREATE UNIQUE INDEX ix_version_version_value ON doc.version (version_value);
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211012145532_initial') THEN
    INSERT INTO "__EFMigrationsHistory" (migration_id, product_version)
    VALUES ('20211012145532_initial', '5.0.11');
    END IF;
END $EF$;
COMMIT;

START TRANSACTION;


DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211105115655_group_id_defvalue') THEN
    ALTER TABLE doc.function_group ALTER COLUMN id SET DEFAULT (gen_random_uuid());
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211105115655_group_id_defvalue') THEN
    INSERT INTO "__EFMigrationsHistory" (migration_id, product_version)
    VALUES ('20211105115655_group_id_defvalue', '5.0.11');
    END IF;
END $EF$;
COMMIT;

START TRANSACTION;


DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211106055612_group_alias') THEN
    ALTER TABLE doc.function_group ADD alias text NOT NULL DEFAULT '';
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211106055612_group_alias') THEN
    CREATE UNIQUE INDEX unq_function_group_alias ON doc.function_group (alias);
    END IF;
END $EF$;

DO $EF$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20211106055612_group_alias') THEN
    INSERT INTO "__EFMigrationsHistory" (migration_id, product_version)
    VALUES ('20211106055612_group_alias', '5.0.11');
    END IF;
END $EF$;
COMMIT;

