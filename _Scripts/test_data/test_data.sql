insert into text.language (code, title) values 
 ('ru', 'Русский') 
,('en', 'English') 
on conflict do nothing;
--insert text.text_resource values default;

with t1 as (
     insert into text.text_resource default values returning id
)
insert into doc.function_name (system_name, title_resource_id)
values ('test_function1', (select id from t1))
on conflict do nothing;

with t1 as (
     insert into text.text_resource default values returning id
)
insert into doc.function_name (system_name, title_resource_id)
values ('test_function2', (select id from t1))
on conflict do nothing;

with t1 as (
     insert into text.text_resource default values returning id
), t2 as (
	insert into text.text_resource default values returning id
), t3 as (
	insert into text.text_resource default values returning id
)
insert into doc."function" (function_name_id, args_count, arguments, "return", description_resource_id)
values
 ((select id from doc.function_name where system_name = 'test_function1'), 0, '[]', '{"type": "String"}', (select id from t1))
,((select id from doc.function_name where system_name = 'test_function2'), 1, '[{"type": "String", "name": "arg1"}]', '{"type": "String"}', (select id from t2))
,((select id from doc.function_name where system_name = 'test_function2'), 2, '[{"type": "String", "name": "arg1"}, {"type": "Integer", "name": "arg2"}]', '{"type": "String"}', (select id from t3))
on conflict do nothing;

-- test_function1 texts
with settings as(
  select
    (select id from text.language where code = 'ru') as ru
   ,(select id from text.language where code = 'en') as en
),
fn as (
  select * from doc.function_name where system_name = 'test_function1'
),
f as (
  select * from doc."function" where function_name_id = (select id from fn) and args_count = 0
)
insert into text.text_resource_row (text_resource_id, language_id, text) values
 ((select title_resource_id from fn), (select ru from settings), 'Заголовок test function 1')
,((select title_resource_id from fn), (select en from settings), 'Title test function 1')
,((select description_resource_id from f), (select ru from settings), 'Описание без аргументов test function 1')
,((select description_resource_id from f), (select en from settings), 'Description without args function 1')
;

-- test_function2 texts
with settings as(
  select
    (select id from text.language where code = 'ru') as ru
   ,(select id from text.language where code = 'en') as en
),
fn as (
  select * from doc.function_name where system_name = 'test_function2'
),
f as (
  select * from doc."function" where function_name_id = (select id from fn) and args_count = 1
)
insert into text.text_resource_row (text_resource_id, language_id, text) values
 ((select title_resource_id from fn), (select ru from settings), 'Заголовок test function 2')
,((select title_resource_id from fn), (select en from settings), 'Title test function 2')
,((select description_resource_id from f), (select ru from settings), 'Описание 1 аргумент test function 2')
,((select description_resource_id from f), (select en from settings), 'Description 1 arg function 2')
;

-- test_function2 texts
with settings as(
  select
    (select id from text.language where code = 'ru') as ru
   ,(select id from text.language where code = 'en') as en
),
fn as (
  select * from doc.function_name where system_name = 'test_function2'
),
f as (
  select * from doc."function" where function_name_id = (select id from fn) and args_count = 2
)
insert into text.text_resource_row (text_resource_id, language_id, text) values
((select description_resource_id from f), (select en from settings), 'Description 2 arg function 2')
;

-- test_function3 
with settings as(
  select
    (select id from text.language where code = 'ru') as ru
   ,(select id from text.language where code = 'en') as en
),
title as (
  insert into text.text_resource default values returning id
),
descr as (
  insert into text.text_resource default values returning id
),
fn as (
  insert into doc.function_name (system_name, title_resource_id)
  values ('test_function3', (select id from title)) 
  returning id
),
f as (
  insert into doc."function" (function_name_id, args_count, arguments, "return", description_resource_id)
  values ((select id from fn), 0, '[]', '{"type": "String"}', (select id from descr))
 returning id
)
insert into text.text_resource_row (text_resource_id, language_id, text) values
 ((select id from title), (select en from settings), 'Title test function 3')
,((select id from descr), (select en from settings), 'Descr test function 3')
;

-- versions
insert into doc.version (version_value) values ('{1,0,0}'),('{1,0,1}') on conflict do nothing;

-- version assignation
with fn as (
  select 
     (select id from doc.function_name where system_name = 'test_function1') as f1
    ,(select id from doc.function_name where system_name = 'test_function2') as f2
    ,(select id from doc.function_name where system_name = 'test_function3') as f3
),
f as (
  select
     (select id from doc."function" where function_name_id = (select f1 from fn) and args_count = 0) as f1_0
    ,(select id from doc."function" where function_name_id = (select f2 from fn) and args_count = 1) as f2_1
    ,(select id from doc."function" where function_name_id = (select f2 from fn) and args_count = 2) as f2_2
    ,(select id from doc."function" where function_name_id = (select f3 from fn) and args_count = 0) as f3_0
)
insert into doc.function_version_assignment (function_id, version_id) values 
 ((select f1_0 from f), (select id from doc.version where version_value = array[1,0,0]))
,((select f2_1 from f), (select id from doc.version where version_value = array[1,0,0]))
,((select f2_2 from f), (select id from doc.version where version_value = array[1,0,1]))
,((select f3_0 from f), (select id from doc.version where version_value = array[1,0,1]))
;

-- *** groups ***
with langs as (
  select 
     (select id from text.language where code='ru') as ru
    ,(select id from text.language where code='en') as en
),
t1 as (insert into text.text_resource default values returning id),
t2 as (insert into text.text_resource default values returning id),
g1 as (insert into doc.function_group (alias, title_resource_id) values ('group1', (select id from t1)) returning id),
g2 as (insert into doc.function_group (alias, title_resource_id) values ('group2', (select id from t2)) returning id)
insert into text.text_resource_row(text_resource_id, language_id, text) values 
 ((select id from t1), (select ru from langs), 'Группа 1')
,((select id from t1), (select en from langs), 'Group 1')
,((select id from t2), (select ru from langs), 'Группа 2')
,((select id from t2), (select en from langs), 'Group 2')
;

-- assign groups to formulas
with settings as (
  select 
     (select id from doc.function_name where system_name = 'test_function1') as f1
    ,(select id from doc.function_name where system_name = 'test_function2') as f2
    ,(select id from doc.function_group where alias = 'group1') as g1
    ,(select id from doc.function_group where alias = 'group2') as g2
)
insert into doc.function_group_function_name (functions_id, groups_id) values
 ((select f1 from settings), (select g1 from settings))
,((select f2 from settings), (select g2 from settings))
;

-- projects
with langs as (
  select 
     (select id from text.language where code='ru') as ru
    ,(select id from text.language where code='en') as en
),
t1 as (insert into text.text_resource default values returning id),
t2 as (insert into text.text_resource default values returning id),
p1 as (insert into project.project (system_name, title_resource_id) values ('project1', (select id from t1)) returning id),
p2 as (insert into project.project (system_name, title_resource_id) values ('project2', (select id from t2)) returning id)
insert into text.text_resource_row(text_resource_id, language_id, text) values 
 ((select id from t1), (select ru from langs), 'Проект 1')
,((select id from t1), (select en from langs), 'Project 1')
,((select id from t2), (select ru from langs), 'Проект 2')
,((select id from t2), (select en from langs), 'Project 2')
;

-- project versions
with settings as (
  select
     (select id from project.project where system_name = 'project1') as prj1
    ,(select id from project.project where system_name = 'project2') as prj2
)
insert into project.project_version (project_id, version, library_version) values
 ((select prj1 from settings), array[1,0,0], array[1,0,0])  -- test_function1(0) && test_function2(1)
,((select prj1 from settings), array[2,0,0], array[1,0,1])  -- test_function1(0) && test_function2(1, 2) && test_function3(0)
,((select prj2 from settings), array[1,0,0], array[1,0,0])  -- test_function1(0) && test_function2(1)
;